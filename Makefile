all: run cl

cl:
	@rm -rf .\__pycache__
	@rm -rf .\mvcamera\__pycache__

git:
	rm ./commit.log
	git log --pretty=format:"%ai,%an:%s" --since="500 day ago" >>./commit.log