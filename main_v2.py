#=================================================================
#   Finding something good without looking for it!
#
#   File_Name   ：main_V2.py
#   Author_Name ：HuJinzhou
#   Created Time：2023年10月31日
#   Description ：
#                   GUI界面根窗口及控制多级界面串口
#=================================================================

import Macro_Variable as mvar
import tkinter as tk
import serial
from tkinter import E, W
import threading
from EnvLine import EnvLine
import time
import Create_Frame2
import Create_Frame1
import Ser_Commu

############################################################
##                       根窗口声明                        ##
############################################################
root_window = tk.Tk()
root_window.iconbitmap(mvar.ROOT_ICON_PATH)     # 设置GUI窗口的图标 & 标题（左上角）
root_window.title(mvar.ROOT_WINDOW_TITLE)
# 设置GUI界面窗口大小并居中屏幕
width = mvar.ROOT_WIDTH
height = mvar.ROOT_HEIGHT
screenwidth = root_window.winfo_screenwidth()
screenheight = root_window.winfo_screenheight()
size_geo = '%dx%d+%d+%d' %(width, height, (screenwidth - width)/2, (screenheight - height)/2)
root_window.geometry(size_geo)
root_window['background'] = "#f5f3f2"
# 设置主窗口的标题'#2f3542', font=('Times Extra Bold', 36),width=40,height=1).grid(row=0,column=4)

############################################################
##!                        配置串口                        ##
############################################################
ser = serial.Serial(mvar.PORT_NAME, 9600, timeout=5)
if ser.isOpen():
    print("Success Open Serial!",'\n', ser.name)
else:
    print("!!!!!! FAIL OPEN SERIAL !!!!!!")


############################################################
##        实时曲线绘制, 调用 usb_read()读出串口的数据        ##
############################################################

# 将读到的数据存入到数组中
runFlag = True
rhData   = []
tempData = []
platData = []
pdmsData = []
waterData = []

def upData():
    while runFlag:
        try:
            rh_read, temp_read, plat_read, pdms_read, water_read = Ser_Commu.Ser_Commu().usb_read(para_ser=ser)
        except:
            print("Wrong this time!")
        rhData.append(rh_read)
        tempData.append(temp_read)
        platData.append(plat_read)
        pdmsData.append(pdms_read)
        waterData.append(water_read)

        if mvar.MESS_INFO_DEBUG:
            print("__DEBUG_IN_upData__")
            print("RH:",rhData,"\n")
            print("Temp:",tempData,"\n")
            print("Plat:",platData,"\n")
            print("PDMS:",pdmsData,"\n")
            print("Water:",waterData,"\n\n")

        time.sleep(3)
    return rhData, tempData, platData, pdmsData, waterData

# 更新窗口
def updataWindow():
    if runFlag:
        tempGraphLabel.create_matplotlib(rhData, tempData, platData, pdmsData, waterData)        # 更新曲线
    root_window.after(2000, updataWindow)               # 2s更新画布

# 创建控件
lineFrame = tk.Frame(root_window).grid(row=2,column=0,columnspan=3)      # 创建图表控件
tempGraphLabel = EnvLine(lineFrame, rhData, tempData, platData, pdmsData, waterData)    # 在frame上例化动态曲线绘制模块

recv_data = threading.Thread(target=upData)     # 开启线程
recv_data.start()

button_next = tk.Button(root_window, text='NEXT', font=('Times Extra Bold', 20), bg='#c12c1f', command=lambda:[Create_Frame1.Create_Frame1().destory_frame(),Create_Frame2.Create_Frame2().create_frame(root_window,para_ser=ser)]).grid(row=6,column=2,sticky=E)

button_back = tk.Button(root_window, text='BACK', font=('Times Extra Bold', 20), bg='#c12c1f', command=lambda:[Create_Frame2.Create_Frame2().destory_frame(),Create_Frame1.Create_Frame1().create_f1(root_window,para_ser=ser)]).grid(row=6,column=0,sticky=W)

updataWindow()      # 更新画布

Create_Frame1.Create_Frame1().create_f1(root_window,ser)     # 显示参数配置frame

root_window.mainloop()