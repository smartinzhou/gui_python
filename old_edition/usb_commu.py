import serial

# read
def usb_read(ser):

    # USB串口读数据
    a=ser.read_until(expected=b'}')
    rh_read = (a[1] + a[2])/2               # 环境湿度，两个湿度传感器取平均
    temp_read = (a[3] + a[4])/2             # 环境温度，两个温度传感器取平均
    plat_read = a[5] + round((a[6]/255),2)      # 平台温度，整数与小数部分数据，保留两位小数
    pdms_read = a[7] + round((a[8]/255),2)      # PDMS温度
    water_read = a[9] + round((a[10]/255),2)    # 水温

    # print(a)
    # print('系统环境湿度：', rh_read, '%')
    # print('系统环境温度：', temp_read, '℃')
    # print('系统平台温度：', plat_read, '℃')
    # print('系统PDMS温度：', pdms_read, '℃')
    # print('系统水温：', water_read, '℃')
    # print('串口通信打开:',ser.isOpen())
    return rh_read, temp_read, plat_read, pdms_read, water_read

# write
def usb_write(ser,*args):
    w_data = list(args)             # 将元组转化为数组列表
    ser.write(w_data)               # 作为整体发送