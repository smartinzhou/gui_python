# 20220810 ———— 设置Gui界面基础布局
from http.client import REQUEST_HEADER_FIELDS_TOO_LARGE
from operator import truth
import tkinter as tk
from matplotlib.animation import ImageMagickFileWriter, ImageMagickWriter
from pyparsing import col
import serial
from tkinter import CENTER, E, FLAT, GROOVE, N, NE, NW, SUNKEN, W, Button, Image, Label, messagebox
from numpy import uint8
from usb_commu import usb_write, usb_read
import threading
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from EnvLine import EnvLine
import time,sys
from PIL import Image, ImageTk

############################################################
##                     声明全局变量                        ##
############################################################

# 声明全局变量
global root_window
global tempGraphLabel, tempData, runFlag
temp_read = 0

############################################################
##                       根窗口声明                        ##
############################################################
root_window = tk.Tk()
root_window.iconbitmap('D:/00link/System Default/photo/icon/lovelearning.ico')     # 设置GUI窗口的图标 & 标题（左上角）
# root_window.title('Cultivation Monitoring System Control')
# 设置GUI界面窗口大小并居中屏幕
width = 1440
height = 900
screenwidth = root_window.winfo_screenwidth()
screenheight = root_window.winfo_screenheight()
size_geo = '%dx%d+%d+%d' %(width, height, (screenwidth - width)/2, (screenheight - height)/2)
root_window.geometry(size_geo)
root_window['background'] = "#f5f3f2"
# 设置主窗口的标题'#2f3542', font=('Times Extra Bold', 36),width=40,height=1).grid(row=0,column=4)

# 声明需要用的图片（全局变量）图片使用到了tk的库，!!!首先需要有根窗口!!!
uvc_img_blue = tk.PhotoImage(file='D:/00link/System Default/photo/icon/icon_cultivate/uvc_4040.gif')
uvc_img_purple = tk.PhotoImage(file='D:/00link/System Default/photo/icon/icon_cultivate/uvc_respond_4040.gif')
light_img_blue = tk.PhotoImage(file='D:/00link/System Default/photo/icon/icon_cultivate/light.gif')
light_img_yellow = tk.PhotoImage(file='D:/00link/System Default/photo/icon/icon_cultivate/light_respond.gif')
buzzer_img_blue = tk.PhotoImage(file='D:/00link/System Default/photo/icon/icon_cultivate/buzzer.gif')
buzzer_img_red = tk.PhotoImage(file='D:/00link/System Default/photo/icon/icon_cultivate/buzzer_respond.gif')

def create_1():
    windows1 = tk.Toplevel()
    windows1.iconbitmap('D:/00link/System Default/photo/icon/lovelearning.ico')     # 设置GUI窗口的图标 & 标题（左上角）
    # root_window.title('Cultivation Monitoring System Control')
    # 设置windows1界面窗口大小并居中屏幕
    windows1.geometry(size_geo)
    windows1['background'] = "#f5f3f2"

    ############################################################
    ##                  UVC、环境温湿度参数设置                 ##
    ############################################################
    # UVC杀菌块
    uvc_Variable = tk.StringVar()            # 设置一个字符串类型的变量
    tk.Label(windows1,text='UVC 杀菌',font=('Times Extra Bold', 20),bg='#42a5f5',width=10,height=2).grid(row=3, column=0,padx=10,pady=5)
    tk.Spinbox(windows1, from_=0, to=15, increment=5, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=uvc_Variable).grid(row=3, column=1)
    tk.Label(windows1, text='min', font=('Times Extra Bold', 20)).grid(row=3, column=2,padx=10)

    # 环境温度
    temp_Variable = tk.StringVar()            # 设置一个字符串类型的变量
    tk.Label(windows1, text='环境-温度', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=4, column=0,padx=10,pady=5)
    tk.Spinbox(windows1, from_=30, to=40, increment=1, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=temp_Variable).grid(row=4, column=1)
    tk.Label(windows1, text='℃', font=('Times Extra Bold', 20)).grid(row=4, column=2,padx=10)

    # 环境湿度
    rh_Variable = tk.StringVar()            # 设置一个字符串类型的变量
    tk.Label(windows1, text='环境-湿度', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=5, column=0,padx=10,pady=5)
    tk.Spinbox(windows1, from_=70, to=100, increment=1, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=rh_Variable).grid(row=5, column=1)
    tk.Label(windows1, text='%', font=('Times Extra Bold', 20)).grid(row=5, column=2,padx=10)

    # 设置点光源模式，0 常亮；1 自动
    light_Variable = tk.StringVar()            # 设置一个字符串类型的变量
    tk.Label(windows1, text='点光源模式', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=6, column=0,padx=10,pady=5)
    tk.Spinbox(windows1, from_=0, to=1, increment=1, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=light_Variable).grid(row=6, column=1)
    tk.Label(windows1, text=' 0- always   1- auto ', wraplength=120,font=('Times Extra Bold', 14)).grid(row=6, column=2,padx=10)

    # 拍摄间隔
    Shooting_interval = tk.StringVar()            # 设置一个字符串类型的变量
    tk.Label(windows1,text='拍摄间隔',font=('Times Extra Bold', 20),bg='#42a5f5',width=10,height=2).grid(row=7, column=0,padx=10,pady=5)
    tk.Spinbox(windows1, from_=0, to=30, increment=5, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=Shooting_interval).grid(row=7, column=1)
    tk.Label(windows1, text='min', font=('Times Extra Bold', 20)).grid(row=7, column=2,padx=10)


    ############################################################
    ##            定义UVC、light、buzzer开关响应函数            ##
    ############################################################
    #! 定义uvc开关响应函数
    # global FLAG_UVC                # 声明UVC开关全局变量，当close button响应时，uvc_close()函数中FLAG_UVC直接给全局变量重新赋值
    def uvc_close():            # 定义uvc关闭按键响应，显示蓝色uvc（关闭状态），再次点击按钮转到open模式，同时返回值FLAG_UVC=0
        global FLAG_UVC
        tk.Button(windows1, image=uvc_img_blue,command=uvc_open).grid(row=8,column=0)
        FLAG_UVC = 0
    def uvc_open():             # 定义uvc打开按键响应，显示紫色uvc（打开状态）,再次点击按钮转到close模式，同时返回值FLAG_UVC=1
        global FLAG_UVC
        tk.Button(windows1, image=uvc_img_purple,command=uvc_close).grid(row=8,column=0)
        FLAG_UVC = 1

    #! 定义light开关响应函数
    # global FLAG_LIGHT                # 声明LIGHT开关全局变量，当close button响应时，light_close()函数中FLAG_LIGHT直接给全局变量重新赋值
    def light_close():            # 定义uvc关闭按键响应，显示蓝色uvc（关闭状态），再次点击按钮转到open模式，同时返回值FLAG_UVC=0
        global FLAG_LIGHT
        tk.Button(windows1, image=light_img_blue,command=light_open).grid(row=8,column=1)
        FLAG_LIGHT = 0
    def light_open():             # 定义uvc打开按键响应，显示紫色uvc（打开状态）,再次点击按钮转到close模式，同时返回值FLAG_UVC=1
        global FLAG_LIGHT
        tk.Button(windows1, image=light_img_yellow,command=light_close).grid(row=8,column=1)
        FLAG_LIGHT = 1

    #! 定义buzzer开关响应函数
    # global FLAG_BUZZER                # 声明LIGHT开关全局变量，当close button响应时，light_close()函数中FLAG_LIGHT直接给全局变量重新赋值
    def buzzer_close():            # 定义uvc关闭按键响应，显示蓝色uvc（关闭状态），再次点击按钮转到open模式，同时返回值FLAG_UVC=0
        global FLAG_BUZZER
        tk.Button(windows1, image=buzzer_img_blue,command=buzzer_open).grid(row=8,column=2)
        FLAG_BUZZER = 0
    def buzzer_open():             # 定义uvc打开按键响应，显示紫色uvc（打开状态）,再次点击按钮转到close模式，同时返回值FLAG_UVC=1
        global FLAG_BUZZER
        tk.Button(windows1, image=buzzer_img_red,command=buzzer_close).grid(row=8,column=2)
        FLAG_BUZZER = 1

    # 设置uvc light buzzer打开开关
    button_uvc = tk.Button(windows1, image=uvc_img_blue,command=uvc_open).grid(row=8,column=0)
    button_light = tk.Button(windows1, image=light_img_blue,command=light_open).grid(row=8,column=1)
    button_buzzer = tk.Button(windows1, image=buzzer_img_blue,command=buzzer_open).grid(row=8,column=2)
    # 设置UVC、点光源、Buzzer开关底部文字指示
    tk.Label(windows1, text='UVC开关', font=('Times Extra Bold', 14),width=10,height=2).grid(row=9, column=0,pady=5)
    tk.Label(windows1, text='点光源开关', font=('Times Extra Bold', 14),width=10,height=2).grid(row=9, column=1,pady=5)
    tk.Label(windows1, text='蜂鸣器开关', font=('Times Extra Bold', 14),width=10,height=2).grid(row=9, column=2,padx=25,pady=5)

    ############################################################
    ## 定义USB串口发送数据传参，并调用 usb_write() 写入数据到串口 ##
    ############################################################
    def usb_action():
        uvc_num =uint8(uvc_Variable.get())
        temp_num = uint8(temp_Variable.get())
        rh_num = uint8(rh_Variable.get())
        light_num = uint8(light_Variable.get())

        # 创建列用来传递列表中的数据(顺序：{ 、湿度、空间温度&水温&平台温度、uvc时间、点光源模式、FLAG_UVC、FLAG_LIGHT、FLAG_BUZZER})
        args_Env = [uint8(123), rh_num, temp_num, temp_num, temp_num, uvc_num, light_num, FLAG_UVC, FLAG_LIGHT, FLAG_BUZZER, uint8(125)]
        print(args_Env)

        usb_write(ser,*args_Env)

    # 设置环境参数发送至USB串口通信按钮
    button_send = tk.Button(windows1,text='Send to USB', font=('Times Extra Bold', 20), bg='#c12c1f', command=usb_action).grid(row=10, column=1,padx=10,pady=10)


    ##################################################
    # 实时曲线绘制
    ############################################################
    ##                调用 usb_read()读出串口的数据             ##
    ############################################################

    # 将读到的数据存入到数组中
    runFlag = True
    rhData   = []
    tempData = []
    platData = []
    pdmsData = []
    waterData = []

    # 更新数据，在次线程中运行
    def upData():
        while runFlag:
            try:
                rh_read, temp_read, plat_read, pdms_read, water_read = usb_read(ser)
            except:
                print("Wrong this time!")
            rhData.append(rh_read)
            tempData.append(temp_read)
            platData.append(plat_read)
            pdmsData.append(pdms_read)
            waterData.append(water_read)
            # 打印读串口数据
            print("RH:",rhData,"\n")
            print("Temp:",tempData,"\n")
            print("Plat:",platData,"\n")
            print("PDMS:",pdmsData,"\n")
            print("Water:",waterData,"\n")

            time.sleep(3)
        return rhData, tempData, platData, pdmsData, waterData

    # 更新窗口
    def updataWindow():
        global tempGraphLabel, runFlag, rhData, tempData, platData, pdmsData, waterData
        if runFlag:
            tempGraphLabel.create_matplotlib(rhData, tempData, platData, pdmsData, waterData)        # 更新曲线
        windows1.after(2000, updataWindow)               # 2s更新画布

    # 创建控件
    # lineFrame = tk.Frame(windows1).grid(row=2,column=4)      # 创建图表控件
    tempGraphLabel = EnvLine(windows1, rhData, tempData, platData, pdmsData, waterData)

    recv_data = threading.Thread(target=upData)     # 开启线程
    recv_data.start()

    updataWindow()      # 更新画布

    windows1.mainloop()



# 创建windows2
def create_2():
        windows2 = tk.Toplevel()
        windows2.geometry(size_geo)
        windows2['background'] = "#f5f3f2"

        ############################################################
        ##                     显示图像采集界面                    ##
        ############################################################
        img = None
        img_show = None
        def show():
            global img
            global img_show
            img = Image.open('E:/XUT/2022/2022比赛/2022研电赛/04GUI_0810/temp/11.png')
            img_show = ImageTk.PhotoImage(img)            # 使用 PIL模块的PhotoImage打开
            tk.Label(windows2, image = img_show, width=600,height=300,).grid(row=1,column=4)
        button_show = tk.Button(windows2, text='SHOW_IMG', font=('Times Extra Bold', 20), bg='#c12c1f', command=show).grid(row=2,column=4,padx=25,pady=5)

        windows2.mainloop()



############################################################
##                          配置串口                       ##
############################################################
ser = serial.Serial('COM4', 9600, timeout=5)
if ser.isOpen():
    print("Success Open Serial!",'\n', ser.name)
else:
    print("!!!!!! FAIL OPEN SERIAL !!!!!!")

button_home = tk.Button(root_window, text='HOME', font=('Times Extra Bold', 20), bg='#c12c1f', command=create_1).grid(row=0,column=0,padx=25,pady=5)
button_next = tk.Button(root_window, text='NEXT', font=('Times Extra Bold', 20), bg='#c12c1f', command=create_2).grid(row=0,column=4,padx=25,pady=5)

root_window.mainloop()