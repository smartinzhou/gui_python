#=========================================================================================
#   Finding something good without looking for it!
#
#   File_Name   ：Create_Frame1.py
#   Author_Name ：HuJinzhou
#   Created Time：2023年10月31日
#   Description ：
#                   创建Frame1：
#                       1. Frame1所需的固定变量
#                       2. 创建frame1基础组件:UVC、环境温、湿度参数设置
#                       3. 定义UVC、light、buzzer开关响应函数
#                       4. 通过USB串口发送数据传参，并调用 usb_write() 写入数据到串口
#=========================================================================================

import Macro_Variable as mvar
import Ser_Commu
import tkinter as tk
from numpy import uint8


class Create_Frame1:
    def __init__(self):
        # 声明需要用的图片（全局变量）图片使用到了tk的库，!!!首先需要有根窗口!!!
        self.uvc_img_blue = tk.PhotoImage(file=mvar.UVC_IMG_BLUE)
        self.uvc_img_purple = tk.PhotoImage(file=mvar.UVC_IMG_PURPLE)
        self.light_img_blue = tk.PhotoImage(file=mvar.LIGHT_IMG_BLUE)
        self.light_img_yellow = tk.PhotoImage(file=mvar.LIGHT_IMG_YELLOW)
        self.buzzer_img_blue = tk.PhotoImage(file=mvar.BUZZER_IMG_BLUE)
        self.buzzer_img_red = tk.PhotoImage(file=mvar.BUZZER_IMG_RED)

        self.FLAG_UVC = 0
        self.FLAG_LIGHT = 0
        self.FLAG_BUZZER = 0

    def create_f1(self, para_window, para_ser):
        # 创建frame1
        global frame1
        frame1 = tk.Frame(para_window, bg='#f0f0f0')
        frame1.grid(row=3,column=0,columnspan=3)
        ############################################################
        ##                 UVC、环境温、湿度参数设置                ##
        ############################################################
        uvc_Variable = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame1,text='UVC 杀菌',font=('Times Extra Bold', 20),bg='#42a5f5',width=10,height=2).grid(row=3, column=0,padx=30,pady=5)
        tk.Spinbox(frame1, from_=0, to=15, increment=5, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=uvc_Variable).grid(row=3,   column=1)
        tk.Label(frame1, text='min', font=('Times Extra Bold', 20)).grid(row=3, column=2,padx=10)

        # 环境温度
        temp_Variable = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame1, text='环境-温度', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=4, column=0,padx=30,pady=5)
        tk.Spinbox(frame1, from_=30, to=40, increment=1, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=temp_Variable).grid(row=4,     column=1)
        tk.Label(frame1, text='℃', font=('Times Extra Bold', 20)).grid(row=4, column=2,padx=10)

        # 环境湿度
        rh_Variable = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame1, text='环境-湿度', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=5, column=0,padx=30,pady=5)
        tk.Spinbox(frame1, from_=70, to=100, increment=1, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=rh_Variable).grid(row=5,  column=1)
        tk.Label(frame1, text='%', font=('Times Extra Bold', 20)).grid(row=5, column=2,padx=10)

        # 设置点光源模式，0 常亮；1 自动
        light_Variable = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame1, text='点光源模式', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=6, column=0,padx=30,pady=5)
        tk.Spinbox(frame1, from_=0, to=1, increment=1, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=light_Variable).grid(row=6,  column=1)
        tk.Label(frame1, text=' 0- always   1- auto ', wraplength=120,font=('Times Extra Bold', 14)).grid(row=6, column=2,padx=10)

        # 拍摄间隔
        Shooting_interval = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame1,text='拍摄间隔',font=('Times Extra Bold', 20),bg='#42a5f5',width=10,height=2).grid(row=7, column=0,padx=30,pady=5)
        tk.Spinbox(frame1, from_=0, to=30, increment=5, borderwidth=2, width=4, font=('Times Extra Bold', 36), textvariable=Shooting_interval).grid(row=7,  column=1)
        tk.Label(frame1, text='min', font=('Times Extra Bold', 20)).grid(row=7, column=2,padx=10)

        ############################################################
        ##            定义UVC、light、buzzer开关响应函数            ##
        ############################################################
        #! 定义uvc开关响应函数
        # global FLAG_UVC                # 声明UVC开关全局变量，当close button响应时，uvc_close()函数中FLAG_UVC直接给全局变量重新赋值
        def uvc_close():            # 定义uvc关闭按键响应，显示蓝色uvc（关闭状态），再次点击按钮转到open模式，同时返回值FLAG_UVC=0
            # global FLAG_UVC
            tk.Button(frame1, image=self.uvc_img_blue,command=uvc_open).grid(row=8,column=0)
            self.FLAG_UVC = 0
        def uvc_open():             # 定义uvc打开按键响应，显示紫色uvc（打开状态）,再次点击按钮转到close模式，同时返回值FLAG_UVC=1
            # global FLAG_UVC
            tk.Button(frame1, image=self.uvc_img_purple,command=uvc_close).grid(row=8,column=0)
            self.FLAG_UVC = 1

        #! 定义light开关响应函数
        # global FLAG_LIGHT                # 声明LIGHT开关全局变量，当close button响应时，light_close()函数中FLAG_LIGHT直接给全局变量重新赋值
        def light_close():            # 定义uvc关闭按键响应，显示蓝色uvc（关闭状态），再次点击按钮转到open模式，同时返回值FLAG_UVC=0
            # global FLAG_LIGHT
            tk.Button(frame1, image=self.light_img_blue,command=light_open).grid(row=8,column=1)
            self.FLAG_LIGHT = 0
        def light_open():             # 定义uvc打开按键响应，显示紫色uvc（打开状态）,再次点击按钮转到close模式，同时返回值  FLAG_UVC=1
            # global FLAG_LIGHT
            tk.Button(frame1, image=self.light_img_yellow,command=light_close).grid(row=8,column=1)
            self.FLAG_LIGHT = 1

        #! 定义buzzer开关响应函数
        # global FLAG_BUZZER                # 声明LIGHT开关全局变量，当close button响应时，light_close()函数中FLAG_LIGHT直接给全局变量重新赋值
        def buzzer_close():            # 定义uvc关闭按键响应，显示蓝色uvc（关闭状态），再次点击按钮转到open模式，同时返回值 FLAG_UVC=0
            # global FLAG_BUZZER
            tk.Button(frame1, image=self.buzzer_img_blue,command=buzzer_open).grid(row=8,column=2)
            self.FLAG_BUZZER = 0
        def buzzer_open():             # 定义uvc打开按键响应，显示紫色uvc（打开状态）,再次点击按钮转到close模式，同时返回值 FLAG_UVC=1
            # global FLAG_BUZZER
            tk.Button(frame1, image=self.buzzer_img_red,command=buzzer_close).grid(row=8,column=2)
            self.FLAG_BUZZER = 1

        # 设置uvc light buzzer打开开关
        button_uvc = tk.Button(frame1, image=self.uvc_img_blue,command=uvc_open).grid(row=8,column=0)
        button_light = tk.Button(frame1, image=self.light_img_blue,command=light_open).grid(row=8,column=1)
        button_buzzer = tk.Button(frame1, image=self.buzzer_img_blue,command=buzzer_open).grid(row=8,column=2)
        # 设置UVC、点光源、Buzzer开关底部文字指示
        tk.Label(frame1, text='UVC开关', font=('Times Extra Bold', 14),width=10,height=2).grid(row=9, column=0,pady=5)
        tk.Label(frame1, text='点光源开关', font=('Times Extra Bold', 14),width=10,height=2).grid(row=9, column=1,pady=5)
        tk.Label(frame1, text='蜂鸣器开关', font=('Times Extra Bold', 14),width=10,height=2).grid(row=9, column=2,padx=25,pady=5)

        ############################################################
        ## 定义USB串口发送数据传参，并调用 usb_write() 写入数据到串口 ##
        ############################################################
        def usb_action():
            uvc_num =uint8(uvc_Variable.get())
            temp_num = uint8(temp_Variable.get())
            rh_num = uint8(rh_Variable.get())
            light_num = uint8(light_Variable.get())
            # 创建列用来传递列表中的数据(顺序：{ 、湿度、空间温度&水温&平台温度、uvc时间、点光源模式、FLAG_UVC、FLAG_LIGHT、FLAG_BUZZER})
            args_Env = [uint8(123), rh_num, temp_num, temp_num, temp_num, uvc_num, light_num, self.FLAG_UVC, self.FLAG_LIGHT, self.FLAG_BUZZER, uint8(125)]

            Ser_Commu.Ser_Commu().usb_write(*args_Env, para_ser=para_ser)
            if mvar.MESS_INFO_DEBUG:
                print('__DEBUG_IN_USB_ACTION__',args_Env)

        # 设置环境参数发送至USB串口通信按钮
        button_send = tk.Button(frame1,text='Send to USB', font=('Times Extra Bold', 20), bg='#c12c1f', command=usb_action).grid(row=10, column=1,padx=10,pady=10)

    def destory_frame(self):
        frame1.destroy()