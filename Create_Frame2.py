#=================================================================
#   Finding something good without looking for it!
#
#   File_Name   ：Create_Frame2.py
#   Author_Name ：HuJinzhou
#   Created Time：2023年10月31日
#   Description ：
#                   创建Frame2：
#                       1. Frame2所需的固定变量
#                       2. 创建frame2基础框架及button
#                       3. py调用matlab显示图像采集界面
#                       4. py调用matlab进行衍射恢复处理
#=================================================================

import Macro_Variable as mvar
import tkinter as tk
from tkinter import E, W
from PIL import Image, ImageTk


class Create_Frame2:
    def __init__(self):
        self.bg_frame = '#f0f0f0'

    def create_frame(self,para_window,para_ser):
        global frame2
        frame2 = tk.Frame(para_window, bg=self.bg_frame)
        frame2.grid(row=3,column=0,columnspan=3)

        ############################################################
        ##            点光源波长、像素尺寸、搜索左/右区间            ##
        ############################################################
        uvc_Variable = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame2,text='点光源波长',font=('Times Extra Bold', 20),bg='#42a5f5',width=10,height=2).grid(row=3, column=0, sticky=W)
        tk.Spinbox(frame2, from_=400, to=600, increment=10, borderwidth=2, width=5, font=('Times Extra Bold', 30), textvariable=uvc_Variable).grid(row=3,   column=1, sticky=E)
        tk.Label(frame2, text='nm', font=('Times Extra Bold', 20)).grid(row=3, column=2)

        # 像素尺寸 um
        temp_Variable = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame2, text='像素尺寸', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=4, column=0, sticky=W)
        tk.Spinbox(frame2, from_=1.34, to=3.0, increment=0.02, borderwidth=2, width=5, font=('Times Extra Bold', 30), textvariable=temp_Variable).grid(row=4, column=1, sticky=E)
        tk.Label(frame2, text='um', font=('Times Extra Bold', 20)).grid(row=4, column=2)

        # 搜索左区间 mm
        rh_Variable = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame2, text='搜索左区间', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=5, column=0, sticky=W)
        tk.Spinbox(frame2, from_=0.4, to=2.0, increment=0.1, borderwidth=2, width=5, font=('Times Extra Bold', 30), textvariable=rh_Variable).grid(row=5,  column=1)
        tk.Label(frame2, text='mm', font=('Times Extra Bold', 20)).grid(row=5, column=2)

        # 搜索右区间 mm
        light_Variable = tk.StringVar()            # 设置一个字符串类型的变量
        tk.Label(frame2, text='搜索右区间', font=('Times Extra Bold', 20), bg='#42a5f5',width=10,height=2).grid(row=6, column=0, sticky=W)
        tk.Spinbox(frame2, from_=2.0, to=10.0, increment=0.5, borderwidth=2, width=5, font=('Times Extra Bold', 30), textvariable=light_Variable).grid(row=6,  column=1)
        tk.Label(frame2, text='mm', wraplength=120,font=('Times Extra Bold', 20)).grid(row=6, column=2)

        button_OpenCam = tk.Button(frame2, text=' Open_Cam', font=('Times Extra Bold', 20), bg='#42a5f5', command=(self.OpenCamera)).grid(row=7,column=0,sticky=W)
        button_Show = tk.Button(frame2, text='Show_IMG', font=('Times Extra Bold', 20), bg='#c12c1f', command=(self.show_img)).grid(row=7,column=2,sticky=E)

        button_diff = tk.Button(frame2, text=' DIFF_REV', font=('Times Extra Bold', 20), bg='#42a5f5', command=self.cmd_run_DiffRev).grid(row=8,column=0,sticky=W)
        button_Show_Rev = tk.Button(frame2, text='Show_Rev', font=('Times Extra Bold', 20), bg='#c12c1f', command=self.OpenDIFFED).grid(row=8,column=2,sticky=E)

    ############################################################
    ##               打开Camera,显示图像采集界面                ##
    ############################################################
    def OpenCamera(self):

        # import matlab.engine
        # eng = matlab.engine.start_matlab("-desktop")
        # eng.cd(mvar.SERIAL_FUN_PATH,nargout=0)

        # eng.Cameralib(nargout=0)        # 通过python执行cmd命令调用matlab执行.m函数

        # 执行Camera控制程序
        with open('./main_opencam.py', encoding='utf-8') as f:
            exec(f.read())

    ############################################################
    ##               调用matalb进行衍射恢复处理                 ##
    ############################################################
    def cmd_run_DiffRev(self):
        # import os
        # cmd_path = "cd E:/Python_Code/gui_python/diff_rev/"
        # cmd_str = "matlab -r \"main_patch\""
        # cmd_all = cmd_path + "&&" + cmd_str
        # # from subprocess import run
        # # run(cmd_str, shell=True)
        # os.system(cmd_all)

        import matlab.engine
        eng = matlab.engine.start_matlab("-desktop")
        eng.cd(mvar.DIFF_REV_PATH,nargout=0)

        eng.main_patch(nargout=0)

    def show_img(self):
        global img
        global img_show
        # save_path = 'E:/Python_Code/gui_python/diff_rev/picture/img_show.png'
        img = Image.open(mvar.SAVE_OPEN_IMG)
        img = img.resize((int(img.size[0]/2),int(img.size[1]/2)))
        img_show = ImageTk.PhotoImage(img)            # 使用 PIL模块的PhotoImage打开
        tk.Label(frame2, image = img_show).grid(row=3,column=0,columnspan=2)
        # img.save(save_path)

    def OpenDIFFED(self):
        global img
        global img_show
        img = Image.open(mvar.DIFFED_IMG_PATG)
        img = img.resize((int(img.size[0]/2),int(img.size[1]/2)))
        img_show = ImageTk.PhotoImage(img)            # 使用 PIL模块的PhotoImage打开
        tk.Label(frame2, image = img_show).grid(row=3,column=0,columnspan=2)

    def destory_frame(self):
        try:
            frame2.destroy()
        except:
            pass