import Camera as cam
import mvsdk
import cv2
import matplotlib.pyplot as plt     # plt 用于显示图片
# import matplotlib.image as mpimg    # mpimg 用于读取图片
import time
import datetime

# 图像文件命名格式
name_list = ['year-month-day-hour-minute-second.bmp',
             'year-month-day-hour-minute.bmp',
             'year-month-day-hour.bmp']

save_path = './data/'
save_name = 2  # 选择文件命名格式
timestep = 50  # 采集时间间隔
time_unit = [1, 0, 0]  # 时间单位 s: 1, m: 60, h: 3600

# 枚举MindVision设备
DevList = mvsdk.CameraEnumerateDevice()
nDev = len(DevList)
Device = 0
if nDev < 1:
    print("     No camera was found!")
    quit()

for i, DevInfo in enumerate(DevList):
    print("{}: {} {}".format(i, DevInfo.GetFriendlyName(), DevInfo.GetPortType()))

Device = int(input("Select camera:(Begin at 0)"))
# i = 0 if nDev == 1 else int(input("Select camera: "))
# DevInfo = DevList[i]
# print(DevInfo)

# 打开相机
print('-->Open Camera:' "{}: {} {}".format(Device, DevInfo.GetFriendlyName(), DevInfo.GetPortType()))
print('++++++++++++++++++++++++++++++++++++\n')
hCamera = cam.OpenCamera(int(Device))

# 让SDK内部捕获图像线程工作
print("--> 让SDK内部捕获图像线程工作")
mvsdk.CameraPlay(hCamera)

# 获取相机特性描述
print("--> 获取相机特性描述",'\n')
cap = mvsdk.CameraGetCapability(hCamera)

# 判断是黑白相机还是彩色相机
monoCamera = (cap.sIspCapacity.bMonoSensor != 0)

# 黑白相机让ISP直接输出MONO数据，而不是扩展成R=G=B的24位灰度
if monoCamera:
    mvsdk.CameraSetIspOutFormat(hCamera, mvsdk.CAMERA_MEDIA_TYPE_MONO8)
else:
    mvsdk.CameraSetIspOutFormat(hCamera, mvsdk.CAMERA_MEDIA_TYPE_BGR8)

# 相机模式切换成连续采集
mvsdk.CameraSetTriggerMode(hCamera, 0)

# 选择分辨率
print('--> Support Resolution:')
print('      -> 1.2592X1944 MAX')
print('      -> 2.自定义')
n_res = int(input('-> Choose Resolution:'))
print('++++++++++++++++++++++++++++++++++++\n')
if n_res == 2:
    re = mvsdk.CameraCustomizeResolution(hCamera)
    mvsdk.CameraSetImageResolution(hCamera, re)

# 设置曝光
print('--> 设置曝光')
print('      -> 1. 自动曝光')
print('      -> 2. 手动曝光')
print('      -> 3. 默认值')
nExp = int(input('-> Choose Exposure Mode:'))

if nExp == 1:
    mvsdk.CameraSetAeState(hCamera, int(1))
elif nExp == 2:
    ExposureTime = int(input('          -> Input Exposure Time(us*1000):'))
    mvsdk.CameraSetAeState(hCamera, int(0))
    mvsdk.CameraSetExposureTime(hCamera, ExposureTime)
    AnalogGain = int(input('          -> Input AnalogGain(int):'))
    print('++++++++++++++++++++++++++++++++++++\n')
    mvsdk.CameraSetAnalogGain(hCamera, AnalogGain)

# 计算RGB buffer所需的大小，这里直接按照相机的最大分辨率来分配
# print('->   计算RGB buffer所需的大小')
if monoCamera:
    FrameBufferSize = cap.sResolutionRange.iWidthMax * cap.sResolutionRange.iHeightMax * (1 if monoCamera else 3)
mvsdk.CameraUnInit(hCamera)

# 功能选择
# FUNEN = [0, 0, 0, 0, 0]
while True:
    print('--> Functions:')
    print('         1.preview')
    print('         2.snapshot')
    print('         3.Timing Acquisition')
    print('         4.Exit')
    fun = int(input('--> Choose:'))
    print('++++++++++++++++++++++++++++++++++++\n')

    if fun == 1:
        # Show frame one by one
        pass
    elif fun == 2:
        # Acquisition a frame0
        hCamera = cam.OpenCamera(int(Device))
        mvsdk.CameraPlay(hCamera)
        mvsdk.CameraSetTriggerMode(hCamera, int(0))
        if monoCamera:
            mvsdk.CameraSetIspOutFormat(hCamera, mvsdk.CAMERA_MEDIA_TYPE_MONO8)
        frameData = cam.GetFrame(hCamera, int(FrameBufferSize))
        mvsdk.CameraUnInit(hCamera)

        # 缩放到原来的1/4
        # x, y = frameData.shape[0:2]
        # plt.figure(figsize=(int(y/8), int(x/8)))
        plt.figure("SnapshotImage")
        plt.imshow(frameData, cmap='gray')
        plt.show()
        # x, y = frameData.shape[0:2]
        # frameData = cv2.resize(frameData,(int(y/4),int(x/4)))
        # image = nparray2mat(Data);
        # cv2.imshow("Press q to end", frameData)
        # if (cv2.waitKey(1) & 0xFF) == ord('q'):
            # quit = True
        # cv2.waitKey()
        # cv2.destroyAllWindows()

    elif fun == 3:
        # 定时采集
        while True:
            print('--> Timing Acquisition:')
            print('         Path:', save_path)
            print('         Name:', name_list[save_name])
            print('         Time Step:', timestep)
            print('         1.Change Save Path')
            print('         2.Change Save Name')
            print('         3.Start')
            sub_fun = int(input('--> Choose:'))
            print('++++++++++++++++++++++++++++++++++++\n')
            num = 0
            if sub_fun == 3:
                # 定时采集图像
                time.sleep(11)      # s
                hCamera = cam.OpenCamera(int(Device))
                mvsdk.CameraPlay(hCamera)
                mvsdk.CameraSetTriggerMode(hCamera, int(0))
                if monoCamera:
                    mvsdk.CameraSetIspOutFormat(hCamera, mvsdk.CAMERA_MEDIA_TYPE_MONO8)

                frameData = cam.GetFrame(hCamera, int(FrameBufferSize))
                # img = nparray2mat(Data)./255
                mvsdk.CameraUnInit(hCamera)

                # 缩放到原来的1/4
                # x, y = frameData.shape[0:2]
                # plt.figure(figsize=(int(y/4), int(x/4)))
                plt.figure("SavedImage")
                plt.imshow(frameData, cmap='gray')
                plt.show()
                # img = cv2.resize(frameData, (int(y / 4), int(x / 4)))
                # cv2.imshow(frameData)

                # 获取当前日期和时间
                now = datetime.datetime.now()
                # 格式化日期和时间为 'yyyy-mm-dd-HH-MM-SS'
                date_time_str = now.strftime('%Y-%m-%d-%H-%M-%S')
                # 创建文件名，拼接日期和时间字符串与'.bmp'扩展名
                imgname = f"{date_time_str}.bmp"

                num += 1
                print(f'    Num: {num}  Image: {imgname}')
                # 保存图像
                cv2.imwrite(save_path + imgname, frameData)
                # cv2.waitKey()
                # cv2.destroyAllWindows()

                # time.sleep(sum([a * b for a, b in zip(timestep, time_unit)]) - 11)
                time.sleep(sum(timestep*time_unit) - 11)
            else:
                break
    elif fun == 4:
        mvsdk.CameraUnInit(hCamera)
        break