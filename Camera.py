#coding=utf-8
import mvsdk
import numpy as np
import platform

def OpenCamera(Device):
	# 枚举相机
	DevList = mvsdk.CameraEnumerateDevice()
	nDev = len(DevList)
	if nDev < 1:
		print("No camera was found!")
		return
	#DeviceName = [];
	#for i, DevInfo in enumerate(DevList):
	#	print("{}: {} {}".format(i, DevInfo.GetFriendlyName(), DevInfo.GetPortType()))
	#	DeviceName.append(DevInfo.GetFriendlyName());
	#i = 0 if nDev == 1 else int(input("Select camera: "))
	# DevInfo = DevList[Device-1]
	DevInfo = DevList[Device]
	#print(DeviceName)
	#print(DevInfo)
	# 打开相机
	hCamera = 0
	try:
		hCamera = mvsdk.CameraInit(DevInfo, -1, -1)
	except mvsdk.CameraException as e:
		print("CameraInit Failed({}): {}".format(e.error_code, e.message) )
		print('++++++++++++++++++++++++++++++++++++\n\n')
		mvsdk.CameraUnInit(hCamera)
		return
	return hCamera

def GetFrame(hCamera,FrameBufferSize):
# 从相机取一帧图片
	pFrameBuffer = mvsdk.CameraAlignMalloc(FrameBufferSize, 16)
	try:
		pRawData, FrameHead = mvsdk.CameraGetImageBuffer(hCamera, 2000)
		mvsdk.CameraImageProcess(hCamera, pRawData, pFrameBuffer, FrameHead)
		mvsdk.CameraReleaseImageBuffer(hCamera, pRawData)
		# windows下取到的图像数据是上下颠倒的，以BMP格式存放。转换成opencv则需要上下翻转成正的
		# linux下直接输出正的，不需要上下翻转
		if platform.system() == "Windows":
			mvsdk.CameraFlipFrameBuffer(pFrameBuffer, FrameHead, 1)
		
		# 此时图片已经存储在pFrameBuffer中，对于彩色相机pFrameBuffer=RGB数据，黑白相机pFrameBuffer=8位灰度数据
		frame_data = (mvsdk.c_ubyte * FrameHead.uBytes).from_address(pFrameBuffer)
		frame = np.frombuffer(frame_data, dtype=np.uint8)
		frame = frame.reshape((FrameHead.iHeight, FrameHead.iWidth, 1 if FrameHead.uiMediaType == mvsdk.CAMERA_MEDIA_TYPE_MONO8 else 3) )
		return frame
	except mvsdk.CameraException as e:
		print("CameraGetImageBuffer failed({}): {}".format(e.error_code, e.message) )
		return -1
	mvsdk.CameraAlignFree(pFrameBuffer)

def CloseCamera(hCamera):
	# 关闭相机
	mvsdk.CameraUnInit(hCamera)
	# 释放帧缓存
	#mvsdk.CameraAlignFree(pFrameBuffer)