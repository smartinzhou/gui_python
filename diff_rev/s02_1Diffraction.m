% 自动聚焦里的衍射恢复函数
function  Diffimae = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,Z,Scale,Iter,Arph)
    [r,c]=size(holoImage);
    r=Scale*r;
    c=Scale*c;
    Lamda= system.lamda*10^(-6) ;
    I1=imresize(holoImage./bkgrdImage,[r c],'bicubic');
    Lox=c*(system.pixel_size*10^(-3)/Scale);
    Loy=r*(system.pixel_size*10^(-3)/Scale);
    fx=linspace(-c/2/Lox,c/2/Lox,c);
    fy=linspace(-r/2/Loy,r/2/Loy,r);
    [fx,fy]=meshgrid(fx,fy);
    k=2*pi/Lamda ;
    Hz_p=exp(j*k*Z.*sqrt(1-(Lamda*fx).^2-(Lamda*fy).^2));  % +z 
    Hz_n=exp(-j*k*Z.*sqrt(1-(Lamda*fx).^2-(Lamda*fy).^2)); % -z
    Hz_n((Lamda*fx).^2+(Lamda*fy).^2>1)=0;
%     figure(111);imshow(Hz_n);
%     figure(222);imshow(Hz_p);
    U1_abs = abs(sqrt(I1));
    Ainfft=fftshift(fft2(U1_abs)); 
    Aoutfft=Ainfft.*Hz_n;
    U0_amp = ifft2(ifftshift(Aoutfft));
%添加
   bkgrdImage = mean(mean(bkgrdImage));
%    figure(131);imshow(((abs(U0_amp-1).^2).*bkgrdImage),[],'colormap', gray);
    showfigure(((abs(U0_amp-1).^2).*bkgrdImage),131,gray,ShowEN)
    U0_amp_angle = angle(U0_amp);
    U0_amp_abs = abs(U0_amp);
    for i=1:Iter
%         U0_amp_abs = 1-U0_amp_abs;%-log(U0_amp_abs); 
        U0_amp_abs = -log(U0_amp_abs); 
%         figure(123);mesh(U0_amp_abs);
%         U0_amp_angle(U0_amp_abs>0) = 0;
        U0_amp_abs(U0_amp_abs<0) = 0;
%         U0_amp_abs = 1-U0_amp_abs;%exp(-U0_amp_abs);
        U0_amp_abs = exp(-U0_amp_abs);
%         figure(1234);mesh(U0_amp_abs);
%         U0_amp_abs(U0_sup1 ==0) = 0;
        U0_new =  U0_amp_abs.*exp(j*U0_amp_angle);
%         figure(3);imshow((1-abs(U0_new)).^2);
        FFTU0_cal1 = fftshift(fft2(U0_new));
        U11_amp = ifft2(ifftshift(FFTU0_cal1.*Hz_p));
        Phase_retrival = angle(U11_amp);
        U11_cal =  (Arph.*abs(U11_amp)+ (1- Arph).*(abs((U1_abs)))).*exp(j*Phase_retrival);
        FFTU1_cal = fftshift(fft2(U11_cal)); 
        U0_amp = ifft2(ifftshift(FFTU1_cal.*Hz_n));
        U0_amp_angle = angle(U0_amp);
        U0_amp_abs = abs(U0_amp);

%       figure(132);imshow(((abs(U0_amp-1).^2).*bkgrdImage),[],'colormap', gray);
        showfigure(((abs(U0_amp-1).^2).*bkgrdImage),132,gray,ShowEN)
%       figure(133);imshow((angle(-U0_amp)),[],'colormap', 1-gray);
        showfigure(angle(-U0_amp),133,1-gray,ShowEN)

    end
%     figure(2);imshow(((abs(U0_amp-1).^2).*backdata)/255,[],'colormap', gray);
%       figure(2);imshow((angle(U0_amp)-min(min(angle(U0_amp))))/(max(max(angle(U0_amp)))-min(min(angle(U0_amp)))));
%         figure(133);imshow((angle(-U0_amp)),[],'colormap', 1-gray);
    
    %U0_amp_angle = angle(U0_amp);
%     O = (abs(U0_amp).^2);
%     Diffimae = ((abs(U0_amp).^2).*backdata);
    Diffimae = ((abs(U0_amp-1).^2).*bkgrdImage);
%     Diffimae = im2uint8(Diffimae-1);


end