%
%    # 主函数
%
% Parameter Config
% 图像文件夹路径
% PicDir ='E:\Desktop\新建文件夹\盖玻片\';
% bkgrdPicPath = 'E:\Desktop\新建文件夹\盖玻片\';

% PicDir = 'F:\研究生\开题\图片\白细胞20221228\picture\';
PicDir = 'E:/Python_Code/gui_python/diff_rev/picture/img_show';
% bkgrdPicPath = 'E:\Desktop\新建文件夹\载玻片\';
%  system: 系统参数,
system.lamda = 470;     %光波长, type: double, unit: nm
system.pixel_size=2.2;  %像素尺寸, type: double, unit: um
system.Z= 0.5;       %像到成像面的距离, type: double, unit: mm
SearchZ = 'wavelet';% wavelet cri contrast CS variance fft Tenengrad Laplacin Brenner Roberts EOG DFT  DCT entropy Range  vollaths
Scale = 4;
Zstep = 0.001;
Zwidth = 0.3;
% Get background image type: double [0 1]
step_case='multi_step';
holoImage = imread([PicDir '.bmp']);        %! 改这里的路径与PicDir的路径

holoImage= im2double(holoImage(:,:,1));
bkgrdImage = mean(mean(holoImage));


figure(1);
imshow(holoImage);
h = imrect;
pos=(wait(h));
%  Range: 截取区域，结构体。(将areaRect赋值给Range)
areaRect.x=pos(1);
areaRect.y=pos(2);
areaRect.w=pos(3);
areaRect.h=pos(4);
roix =round((areaRect.x):(areaRect.x+ areaRect.w));
roiy = round((areaRect.y):(areaRect.y+areaRect.h));

holoImage1 = im2double(holoImage(roiy,roix,1));
%     imwrite(holoImage1,'00.bmp');
bkgrdImage1 = im2double(mean(mean(holoImage1)));
% % % holoImage = imread('11.bmp');
% % % holoImage1 = im2double(holoImage(:,:,1));
% % % bkgrdImage1 = mean(mean(holoImage1));
% % % bkgrdImage=bkgrdImage1 ;

% To disdiffraction, type: double [0 1]

[z_best,realImage] = s02_disdiffraction(holoImage,bkgrdImage,system,step_case,'Scale',Scale,'SearchZ',false,'Zstep',Zstep,'Zwidth',Zwidth,'ShowEN',true);

figure;imagesc(realImage);
% name=strcat(SearchZ,'.bmp');
% imwrite(realImage,name);
% [M,N]=size(realImage);
% realImage=im2uint8(realImage);
% [centroid,m,status] = cellArea(realImage);
% f=zeros(M,N);
%
% for i=1:m
%     x=round((centroid(i,1).Centroid(1,1))/4);
%     y=round((centroid(i,1).Centroid(1,2))/4);
%     ROI_X=x-40:x+40;
%     ROI_Y=y-40:y+40;
%     roi_xy=holoImage(ROI_Y,ROI_X);
%     holo=im2double(roi_xy);
%     bkgr=mean(mean(holo));
%     [z_best,roi_realImage] = s02_disdiffraction(holo,bkgr,holo,bkgr,system,'Scale',Scale,'SearchZ',SearchZ,'Zstep',Zstep,'Zwidth',Zwidth,'ShowEN',true);
% 
% imwrite(roi_realImage,'roi_realImage.bmp')
% roi_realImage=im2uint8(roi_realImage);
%   subplot(1,2,1),imshow(roi_realImage);
% [masked_im,circ_mask] = circle_mask(roi_realImage);% 调用制作圆形掩膜矩阵函数
% subplot(1,2,2),imshow(masked_im);
% masked_im=im2double(masked_im);
% 
% f((4*y-161:4*y+162),(4*x-161:4*x+162))=masked_im;
% figure(12345),imshow(f);
% end
% [mm,nn]=size(f);
% for i=1:mm
%     for j=1:nn
%         if(f(i,j)==0)
%             f(i,j)=im2double(realImage(i,j));
% 
% 
%         end
%     end
% end
% 
% figure(112233)
% imshow(f);
% imwrite(f,'f.bmp')
% h = fspecial('average',3) ;
% hsobel=fspecial('sobel');
% hprewitt=fspecial('prewitt');
% hlaplacian=fspecial('laplacian');
% hlog=fspecial('log');
% % format short;%设置输出数据格式，精度保留六位
% J0=imfilter(f,h);
% imwrite(J0,'J0.bmp')
% 
% figure(876);
% imshow(J0);

