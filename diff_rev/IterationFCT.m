function q_eval = IterationFCT(inputdata,DiffimaeLast,Mode)
    [H,W] = size(inputdata);
    switch(Mode)
        case 'wavelet'
      
            q1 = wavelet(inputdata);
%             q2 = wavelet(DiffimaeLast);
            q_eval = q1;

        case 'cri'
            I_mean = mean(mean(inputdata));
            I_high = inputdata - I_mean;
            I_high_mean = mean(mean(abs(I_high)));
            rms = sqrt(mean(mean(((I_high - I_high_mean).^2))));
            q_eval= (rms/I_high_mean);
%           q_eval= mean(mean(((I_high - I_high_mean).^2)));   %改进平方差

        case 'contrast' %对比度判焦函数
%             g = padarray(inputdata,[1,1],'symmetric','both');
             g=inputdata;
            [r,c]=size(g);
            k=0;
            for i=2:r-1
                for j=2:c-1
                    k=k+ (g(i,j-1)-g(i,j))^2 + (g(i-1,j)-g(i,j))^2 ...
                       + (g(i,j+1)-g(i,j))^2 + (g(i+1,j)-g(i,j))^2;
                end
            end
            q_eval = k/(4*(H-2)*(W-2)+3*(2*(H-2)+2*(W-2))+4*2);
        case 'CS' %余弦分值函数
            U12 = sum(sum(inputdata.*DiffimaeLast));
            U1 = sqrt(sum(sum(inputdata.^2)));
            U2 = sqrt(sum(sum(DiffimaeLast.^2)));
            q_eval = U12/(U1*U2);
        case 'variance'%方差函数
            Imean1 = mean(mean(inputdata));
            U1 = 1/Imean1*sum(sum((inputdata-Imean1).^2));
            q_eval = U1;
        case 'fft' % 频谱分值函数
            F1 = abs(fftshift(fft2(inputdata)));
            F2 = abs(fftshift(fft2(DiffimaeLast)));
            U12 = sum(sum(F1.*F2));
            U1 = sqrt(sum(sum(F1.^2)));
            U2 = sqrt(sum(sum(F2.^2)));
            q_eval = U12/(U1*U2);
        case 'Tenengrad' %Tenengrad判焦
      
            I=inputdata;
            [M,N]=size(I);
            %利用Sobel算子gx,gy与图像做卷积,提取图像水平方向和垂直方向的梯度值
            GX = 0;%图像水平方向的梯度值
            GY = 0;%图像垂直方向的梯度值
            FI = 0;%变量，暂时存储图像清晰度值
            % T  = 0;%设置的阈值
            for x=2:M-1
	             for y=2:N-1
		             GX = I(x-1,y-1)+2*I(x,y+1)+I(x+1,y+1)-I(x-1,y-1)-2*I(x,y-1)-I(x+1,y-1);
		             GY = I(x+1,y-1)+2*I(x+1,y)+I(x+1,y+1)-I(x-1,y-1)-2*I(x-1,y)-I(x-1,y+1);
		             SXY= abs(sqrt(GX*GX+GY*GY)); %某一点的梯度值
		             FI = FI + SXY*SXY; %Tenengrad值定义
                     q_eval = FI/(M*N);
                 end
            end
         case 'Laplacin' %Laplacin判焦函数
      
             I = inputdata;
             [M,N] = size(I);
              FI=0;
              for x=2:M-1
                 for y=2:N-1
                     Lxy=-4*I(x,y)+I(x,y+1)+I(x,y-1)+I(x+1,y)+I(x-1,y);
                     FI=FI+Lxy*Lxy;
                 end
              end
                     q_eval=FI;
        
        case 'Brenner'%计算x方向上相差两个像素点的差分
            I = inputdata;
            [M,N] = size(I);
            FI=0;
            for x=1:M-2      %Brenner函数原理，计算相差两个位置的像素点的灰度值
                for y=1:N
                    FI=FI+(I(x+2,y)-I(x,y))*(I(x+2,y)-I(x,y));
                end
            end
            q_eval=FI;
        case 'Roberts'%对角方向相邻的两像素之差
            I = inputdata;
            [M,N] = size(I);
            FI=0;
            for x=1:M-1
                for y=1:N-1
                    FI= FI + (abs(I(x,y)-I(x+1,y+1))+abs(I(x+1,y)-I(x,y+1)));
                end
            end
           q_eval=FI;
        case 'EOG'%(Energy Of Grad)
            I = inputdata;
            [M,N] = size(I);
            FI=0;
            for x=1:M-1
                for y=1:N-1
                    % x方向和y方向的相邻像素灰度值只差的的平方和作为清晰度值
                    FI=FI+(I(x+1,y)-I(x,y))*(I(x+1,y)-I(x,y))+(I(x,y+1)-I(x,y))*(I(x,y+1)-I(x,y));
                end
            end
            q_eval=FI;
        case'DFT'  %基于二维离散傅里叶变换的图像清晰度评价函数
            I = inputdata;
            [M,N] = size(I);
            fftI = fft2(I);   %进行二维离散傅里叶变换
            sfftI = fftshift(fftI);   %移位，直流分量移到图像中心
            magnitude = abs(sfftI);      %取模值
            FI=0;
            for u=1:M
                for v=1:N
                    FI=FI+sqrt(u*u+v*v)*magnitude(u,v);      %基于离散傅里叶变换的清晰度评价函数
                end
            end
          q_eval=FI/(M*N);
        case 'DCT'%离散余弦变换DCT
            I = inputdata;
%             I=double(I)+10*randn(size(I));
            [M,N] = size(I);
            dctI = dct2(I);   %进行二维离散傅里叶变换
            magnitude = abs(dctI);      %取模值
            FI=0;
            for u=1:M
                for v=1:N
                    FI=FI+(u+v)*magnitude(u,v);      %基于离散傅里叶变换的清晰度评价函数
                end
            end
            q_eval=FI/(M*N);
        case'entropy'%基于信息熵
             I =  inputdata;
             MAX_I=max(max(I));
             I1=I./MAX_I;
             i2=round(I1*255);
             q_eval=1/entr(i2);%取倒数
        case'Range'
            I =  inputdata;
            gray_level = 32; %灰度直方图中划分的灰度等级
            temp=zeros(1,gray_level);
            [count,K] = imhist(I,gray_level);%imhist()画灰度分布直方图,count表示某一灰度区间的像素个数，K表示灰度区间取值
            for y=1:gray_level
                temp(1,y)=count(y)*K(y);
            end
            q_eval=1/(max(temp)-min(temp));  %取倒数
        case'vollaths'%自相关函数，反应空间两点相似性
            I =  inputdata;
            [M N]=size(I);
            FI=0;        %变量，暂时存储每一幅图像的Brenner值
            for x=1:M-2      %Brenner函数原理，计算相差两个位置的像素点的灰度值
                for y=1:N
                    FI=FI+I(x,y)*abs(I(x+1,y)-I(x+2,y));
                end
            end
            q_eval=FI;
        case'image_gray'
            FI=0;
            I = inputdata;
            yy=0.5*max(max(I));
            [M ,N]=size(I);
            for x=1:M     %Brenner函数原理，计算相差两个位置的像素点的灰度值
                for y=1:N
                    if(I(x,y)<=yy)
                     
                        FI=FI+1;

                    end
                end
            end
            q_eval=FI;


   



            
    end

end
