
%    # 02.s02_disdiffraction
%    
% 功能：通过第一帧图像初始化变量
% 
% 变量：
% 
%      holoImage: 全息图像, type: double [0-1]
%      bkgrdImage: 背景图像, type: double [0-1];
%      system: 系统参数,
%           system.lamda  光波长, type: double, unit: nm
%           system.pixelPitch  像素尺寸, type: double, unit: um
%           system.Z 像到成像面的距离, type: double, unit: mm
% 参数：
% 
%      'SearchZ': 自动检索距离Z, false(default)/Methods
%                Methods:'contrast', 'variance', 'wavelet','Laplacin'
%      'Scale': 放大倍数, type: int, 2 (default)
%      'Iter': 迭代次数, type: int, 15 (default)
%      'Arph': 迭代中，幅值计算结果的占比, type: double [0 -1], 0 (default)
%      'ShowEN': figure显示使能标志，false(default)/true
%      'SaveEN: 区域图像保存使能标志，false(default)/true
%      'SavePath': 区域图像保存路径, '' (default)
% 
% 输出：
% realImage1 : 原始图像,焦距(z_best+system.lamda/4*10^(-6)), type: double
%                  [0-1]   获取图像区域使用
% realImage2 : 原始图像,焦距(z_best+system.lamda/2*10^(-6)), type: double [0-1]
%                  [0-1]   获取图像区域内细胞数目使用
% 
% 调用方法：
% 
%      [realImage1,realImage2] = s02_disdiffraction(holoImage,bkgrdImage,system)
%      [realImage1,realImage2] = s02_disdiffraction(holoImage,bkgrdImage,system,'SearchZ','wavelet')
%      [realImage1,realImage2] = s02_disdiffraction(holoImage,bkgrdImage,system,'Scale',4)
%     [realImage1,realImage2] = s02_disdiffraction(holoImage,bkgrdImage,system,'Scale',4,'Iter',10)
%      [realImage1,realImage2] = s02_disdiffraction(holoImage,bkgrdImage,system,'ShowEN',true)
%     [realImage1,realImage2] = s02_disdiffraction(holoImage,bkgrdImage,system,'SaveEN',true,'SavePath1','/path'e,'SavePath2','/path')
%     [realImage1,realImage2] = s02_disdiffraction(holoImage,bkgrdImage,system,'ShowEN',true,'SaveEN',true,'SavePath1','/path'e,'SavePath2','/path')

     
function [z_best,realImage] = s02_disdiffraction(holoImage,bkgrdImage,system,mode,varargin)
    switch(mode)
        case  'signal_step'  
    save_path='C:\Users\yjh\Desktop\开题\图片\玻片白细胞恢复20221122\单细胞多焦距\';
    p = inputParser;
    addParameter(p,'SearchZ', false);  % 自动检索距离Z, false(default)/Methods
    addParameter(p,'Scale',4);  % 放大倍数, type: int, 2 (default)
    addParameter(p,'Iter',15);  % 迭代次数, type: int, 15 (default)
    addParameter(p,'Zstep',0.02);  % 迭代步长, type: float, 0.02 (default)
    addParameter(p,'Zwidth',0.4);  % 迭代宽度, type: float, 0.4 (default)
    addParameter(p,'Arph',0);   % 迭代中，幅值计算结果的占比, type: double [0 -1], 0 (default)
    addParameter(p,'ShowEN',false);     % 设置figure显示使能标志初始值
    addParameter(p,'SaveEN',false);     % 设置区域图像保存使能标志初始值
    addParameter(p,'originalImage_name','diffraction');     % 设置区域图像的名称
    addParameter(p,'SavePath1','./');      % 设置区域图像保存路径初始值 
    parse(p,varargin{:});       % 对输入变量进行解析，如果检测到前面的变量被赋值，则更新变量取值
    SearchZ = p.Results.SearchZ;    % 自动检索距离Z, false(default)/Methods
    Zstep = p.Results.Zstep;  % 迭代步长, type: float, 0.02 (default)
    Zwidth = p.Results.Zwidth;
    Scale = p.Results.Scale;    % 图像放大
    Iter = p.Results.Iter;      % 迭代次数
    Arph = p.Results.Arph;      % 迭代中，幅值计算结果的占比
    ShowEN = p.Results.ShowEN;          % figure显示使能标志
    SaveEN = p.Results.SaveEN;          % 区域图像保存使能标志
    originalImage_name  = p.Results.originalImage_name;          % 区域图像名称标志
    SavePath1 = p.Results.SavePath1;      % 区域图像保存路径使能标志

    holoImage = double(holoImage);%输入图片数据类型double类型
    bkgrdImage = double(bkgrdImage);%输入图片数据类型double类型
%     z_best=system.Z;
N_init = floor(system.Z/(system.lamda*10^(-6)));
z_best= N_init*(system.lamda*10^(-6));

    if(SearchZ~=false)
%         N_init = floor(system.Z/(system.lamda*10^(-6))); % 初始猜测的Z对应到lamda的倍数
        N_step = floor(Zstep/(system.lamda*10^(-6))); % 计算检索补偿对应到lamda的倍数，步长为0.002mm
        N_width = floor(Zwidth/(system.lamda*10^(-6))); % 计算检索半宽度对应到lamda的倍数，半宽度为0.002mm
        Z = system.lamda*10^(-6).*(N_init-N_width:N_step:N_init+N_width);
        q_eval_list=[]; 
        z_list=[];
        T=[];
       
%         DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,Z(1)-N_step*system.lamda*10^(-6),Scale,0,Arph);

        for z= Z
            z_list=[z_list,z];
            DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z-N_step*system.lamda*10^(-6),Scale,0,Arph);
   
            Diffimae = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z,Scale,0,Arph);
%            取图像中间部分使用图像评价函数进行判断
              [h0,w0]=size(DiffimaeLast);
              DiffimaeLast1=DiffimaeLast(round(h0/3):round(h0*2/3),round(w0/3):round(w0*2/3));
              [h1,w1]=size(Diffimae);
              Diffimae1=Diffimae(round(h1/3):round(h1*2/3),round(w1/3):round(w1*2/3));
%                figure(22);imshow(Diffimae1./(max(max(Diffimae1))));

                tic;
                q_eval = IterationFCT(Diffimae1,DiffimaeLast1,SearchZ);
                t_end=toc;
                T=[T,t_end];
                t_sum=sum(T);
               
            DiffimaeLast = Diffimae;
            q_eval_list=[q_eval_list,q_eval];
        end
       fprintf('%f',t_sum);
        [~,finall_index]=find(q_eval_list==max(q_eval_list));%找到q_eval_list列表中最大值对应的位置坐标           
        if(ShowEN)
            figure(141);plot(z_list,q_eval_list);
            title(SearchZ);
            xlabel('重建距离/mm');
            ylabel('图像评价值');
        end
        z_best=z_list(finall_index);
    end
    Diffimae2 = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z_best+system.lamda/4*10^(-6),Scale,Iter,Arph);
    max_Diffimae2=max(max(Diffimae2));
    realImage=Diffimae2./max_Diffimae2;%将需要保存的数据转换为[0,1]的浮点数，或者uint8类型
    imwrite(realImage,'small.bmp');
   case  'multi_step'
       save_path='C:\Users\yjh\Desktop\开题\图片\玻片白细胞恢复20221122\单细胞多焦距\';
    p = inputParser;
    addParameter(p,'SearchZ', false);  % 自动检索距离Z, false(default)/Methods
    addParameter(p,'Scale',4);  % 放大倍数, type: int, 2 (default)
    addParameter(p,'Iter',15);  % 迭代次数, type: int, 15 (default)
    addParameter(p,'Zstep',0.02);  % 迭代步长, type: float, 0.02 (default)
    addParameter(p,'Zwidth',0.4);  % 迭代宽度, type: float, 0.4 (default)
    addParameter(p,'Arph',0);   % 迭代中，幅值计算结果的占比, type: double [0 -1], 0 (default)
    addParameter(p,'ShowEN',false);     % 设置figure显示使能标志初始值
    addParameter(p,'SaveEN',false);     % 设置区域图像保存使能标志初始值
    addParameter(p,'originalImage_name','diffraction');     % 设置区域图像的名称
    addParameter(p,'SavePath1','./');      % 设置区域图像保存路径初始值 
    parse(p,varargin{:});       % 对输入变量进行解析，如果检测到前面的变量被赋值，则更新变量取值
    SearchZ = p.Results.SearchZ;    % 自动检索距离Z, false(default)/Methods
    Zstep = p.Results.Zstep;  % 迭代步长, type: float, 0.02 (default)
    Zwidth = p.Results.Zwidth;
    Scale = p.Results.Scale;    % 图像放大
    Iter = p.Results.Iter;      % 迭代次数
    Arph = p.Results.Arph;      % 迭代中，幅值计算结果的占比
    ShowEN = p.Results.ShowEN;          % figure显示使能标志
    SaveEN = p.Results.SaveEN;          % 区域图像保存使能标志
    originalImage_name  = p.Results.originalImage_name;          % 区域图像名称标志
    SavePath1 = p.Results.SavePath1;      % 区域图像保存路径使能标志

    holoImage = double(holoImage);%输入图片数据类型double类型
    bkgrdImage = double(bkgrdImage);%输入图片数据类型double类型
%     z_best=system.Z;
N_init = floor(system.Z/(system.lamda*10^(-6)));
z_best= N_init*(system.lamda*10^(-6));

    if(SearchZ~=false)
        
        % N_init = floor(system.Z/(system.lamda*10^(-6))); % 初始猜测的Z对应到lamda的倍数
        N_step = floor(Zstep/(system.lamda*10^(-6))); % 计算检索补偿对应到lamda的倍数，步长为0.002mm
        N_width = floor(Zwidth/(system.lamda*10^(-6))); % 计算检索半宽度对应到lamda的倍数，半宽度为0.002mm
        Z = system.lamda*10^(-6).*(N_init-N_width:N_step:N_init+N_width);
        q_eval_list=[]; 
        z_list=[];
        T=[];
       
%         DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,Z(1)-N_step*system.lamda*10^(-6),Scale,0,Arph);

        for z= Z
            z_list=[z_list,z];
            DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z-N_step*system.lamda*10^(-6),Scale,0,Arph);
   
            Diffimae = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z,Scale,0,Arph);
%            取图像中间部分使用图像评价函数进行判断
              [h0,w0]=size(DiffimaeLast);
              DiffimaeLast1=DiffimaeLast(round(h0/3):round(h0*2/3),round(w0/3):round(w0*2/3));
              [h1,w1]=size(Diffimae);
              Diffimae1=Diffimae(round(h1/3):round(h1*2/3),round(w1/3):round(w1*2/3));
%                figure(22);imshow(Diffimae1./(max(max(Diffimae1))));

                tic;
                q_eval = IterationFCT(Diffimae1,DiffimaeLast1,SearchZ);
                t_end=toc;
                T=[T,t_end];
                t_sum=sum(T);
               
            DiffimaeLast = Diffimae;
            q_eval_list=[q_eval_list,q_eval];
        end
       fprintf('%f',t_sum);
        [~,finall_index]=find(q_eval_list==max(q_eval_list));%找到q_eval_list列表中最大值对应的位置坐标           
        if(ShowEN)
            figure(141);plot(z_list,q_eval_list);
            title(SearchZ);
            xlabel('重建距离/mm');
            ylabel('图像评价值');
        end
        z_best=z_list(finall_index);
    end
    Diffimae2 = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z_best+system.lamda/4*10^(-6),Scale,Iter,Arph);
    max_Diffimae2=max(max(Diffimae2));
    realImage=Diffimae2./max_Diffimae2;%将需要保存的数据转换为[0,1]的浮点数，或者uint8类型
    imwrite(realImage,'small.bmp');

    %找最小步长
    
    
    Z_list2=100:50:1000;
    q_eval_list2=[max(q_eval_list)]; 
    z_list2=[0];
    for z2=Z_list2
        z_list2=[z_list2,z2];
            DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z_best+(z2-100)*system.lamda*10^(-6),Scale,0,Arph);
   
            Diffimae = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z_best+(z2)*system.lamda*10^(-6),Scale,0,Arph);
%            取图像中间部分使用图像评价函数进行判断
              [h0,w0]=size(DiffimaeLast);
              DiffimaeLast1=DiffimaeLast(round(h0/3):round(h0*2/3),round(w0/3):round(w0*2/3));
              [h1,w1]=size(Diffimae);
              Diffimae1=Diffimae(round(h1/3):round(h1*2/3),round(w1/3):round(w1*2/3));
%                figure(22);imshow(Diffimae1./(max(max(Diffimae1))));

                
                q_eval2 = IterationFCT(Diffimae1,DiffimaeLast1,SearchZ);
                
               
            DiffimaeLast = Diffimae;
            q_eval_list2=[q_eval_list2,q_eval2];
    end
 
    case  'multi_step1'
    save_path='C:\Users\yjh\Desktop\开题\图片\玻片白细胞恢复20221122\单细胞多焦距\';
    p = inputParser;
    addParameter(p,'SearchZ', false);  % 自动检索距离Z, false(default)/Methods
    addParameter(p,'Scale',4);  % 放大倍数, type: int, 2 (default) 
    addParameter(p,'Iter',15);  % 迭代次数, type: int, 15 (default)
    addParameter(p,'Zstep',0.02);  % 迭代步长, type: float, 0.02 (default)
    addParameter(p,'Zwidth',0.4);  % 迭代宽度, type: float, 0.4 (default)
    addParameter(p,'Arph',0);   % 迭代中，幅值计算结果的占比, type: double [0 -1], 0 (default)
    addParameter(p,'ShowEN',false);     % 设置figure显示使能标志初始值
    addParameter(p,'SaveEN',false);     % 设置区域图像保存使能标志初始值
    addParameter(p,'originalImage_name','diffraction');     % 设置区域图像的名称
    addParameter(p,'SavePath1','./');      % 设置区域图像保存路径初始值 
    parse(p,varargin{:});       % 对输入变量进行解析，如果检测到前面的变量被赋值，则更新变量取值
    SearchZ = p.Results.SearchZ;    % 自动检索距离Z, false(default)/Methods
    Zstep = p.Results.Zstep;  % 迭代步长, type: float, 0.02 (default)
    Zwidth = p.Results.Zwidth;
    Scale = p.Results.Scale;    % 图像放大
    Iter = p.Results.Iter;      % 迭代次数
    Arph = p.Results.Arph;      % 迭代中，幅值计算结果的占比
    ShowEN = p.Results.ShowEN;          % figure显示使能标志
    SaveEN = p.Results.SaveEN;          % 区域图像保存使能标志
    originalImage_name  = p.Results.originalImage_name;          % 区域图像名称标志
    SavePath1 = p.Results.SavePath1;      % 区域图像保存路径使能标志

    holoImage = double(holoImage);%输入图片数据类型double类型
    bkgrdImage = double(bkgrdImage);%输入图片数据类型double类型
%     z_best=system.Z;
N_init = floor(system.Z/(system.lamda*10^(-6)));
z_best= N_init*(system.lamda*10^(-6));

    if(SearchZ~=false)
        
        % N_init = floor(system.Z/(system.lamda*10^(-6))); % 初始猜测的Z对应到lamda的倍数
        N_step = floor(Zstep/(system.lamda*10^(-6))); % 计算检索补偿对应到lamda的倍数，步长为0.002mm
        N_width = floor(Zwidth/(system.lamda*10^(-6))); % 计算检索半宽度对应到lamda的倍数，半宽度为0.002mm
        Z = system.lamda*10^(-6).*(N_init-N_width:N_step:N_init+N_width);
        q_eval_list=[]; 
        
        T=[];
        z0=system.lamda*10^(-6).*(N_init-N_width)-system.lamda*10^(-6).*N_step;
        z1=system.lamda*10^(-6).*(N_init-N_width);
        z_list=[];
        image_value1=0;
        image_value2=0;
        while image_value1<=image_value2
            image_value1=image_value2;
            


       
%         DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,Z(1)-N_step*system.lamda*10^(-6),Scale,0,Arph);

%         for z= Z

            z_list=[z_list,z1];
%             DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z-N_step*system.lamda*10^(-6),Scale,0,Arph);
              DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z0,Scale,0,Arph);
   
            Diffimae = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z1,Scale,0,Arph);
%            取图像中间部分使用图像评价函数进行判断
              [h0,w0]=size(DiffimaeLast);
              DiffimaeLast1=DiffimaeLast(round(h0/3):round(h0*2/3),round(w0/3):round(w0*2/3));
              [h1,w1]=size(Diffimae);
              Diffimae1=Diffimae(round(h1/3):round(h1*2/3),round(w1/3):round(w1*2/3));
%                figure(22);imshow(Diffimae1./(max(max(Diffimae1))));

                tic;
%              q_eval = IterationFCT(Diffimae1,DiffimaeLast1,SearchZ);
              image_value2 = IterationFCT(Diffimae1,DiffimaeLast1,SearchZ);
                t_end=toc;
                T=[T,t_end];
                t_sum=sum(T);
               
            DiffimaeLast = Diffimae;
            q_eval_list=[q_eval_list,image_value2];
            z0=z1;
            z1=z1+system.lamda*10^(-6).*N_step;
            
        end
%        fprintf('%f',t_sum);
    end
        [~,finall_index]=find(q_eval_list==max(q_eval_list));%找到q_eval_list列表中最大值对应的位置坐标           
        if(ShowEN)
            figure(141);plot(z_list,q_eval_list);
            title(SearchZ);
            xlabel('重建距离/mm');
            ylabel('图像评价值');
        end
        z_best=z_list(finall_index);
   
    Diffimae2 = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z_best+system.lamda/4*10^(-6),Scale,Iter,Arph);
    max_Diffimae2=max(max(Diffimae2));
    realImage=Diffimae2./max_Diffimae2;%将需要保存的数据转换为[0,1]的浮点数，或者uint8类型
    imwrite(realImage,'small.bmp');

    %找最小步长
    
    
    Z_list2=100:50:1000;
    q_eval_list2=[max(q_eval_list)]; 
    z_list2=0;
    for z2=Z_list2
        z_list2=[z_list2,z2];
            DiffimaeLast = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z_best+(z2-100)*system.lamda*10^(-6),Scale,0,Arph);
   
            Diffimae = s02_1Diffraction(holoImage,bkgrdImage,ShowEN,system,z_best+(z2)*system.lamda*10^(-6),Scale,0,Arph);
%            取图像中间部分使用图像评价函数进行判断
              [h0,w0]=size(DiffimaeLast);
              DiffimaeLast1=DiffimaeLast(round(h0/3):round(h0*2/3),round(w0/3):round(w0*2/3));
              [h1,w1]=size(Diffimae);
              Diffimae1=Diffimae(round(h1/3):round(h1*2/3),round(w1/3):round(w1*2/3));
%                figure(22);imshow(Diffimae1./(max(max(Diffimae1))));

                
                q_eval2 = IterationFCT(Diffimae1,DiffimaeLast1,SearchZ);
                
               
            DiffimaeLast = Diffimae;
            q_eval_list2=[q_eval_list2,q_eval2];
    end
       % [~,finall_index]=find(q_eval_list==max(q_eval_list));%找到q_eval_list列表中最大值对应的位置坐标           
        if(ShowEN)
            figure(555);plot(z_list2,q_eval_list2);
            title(SearchZ);
            xlabel('重建距离/mm');
            ylabel('图像评价值');

        end

        end
       
    

    
      

 
