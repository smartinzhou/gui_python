%
%    # 01.s01_getArea
%    功能：获取计算的细胞区域
% 
%    变量：
% 
%      originalImage： 原始图像,type: double [0-1]
% 
%    参数：
% 
%      'Range': 截取区域，结构体。false(default)/range
%           areaRect.x,areaRect.y 区域左上角坐标
%           areaRect.h,areaRect.w 区域高宽
%      'ShowEN': figure显示使能标志，false(default)/true
%      'SaveEN: 区域图像保存使能标志，false(default)/true
%      'SavePath': 区域图像保存路径, '' (default)
% 
%    输出：
% 
%      imageArea 细胞区域图像,type: double [0-1]
% 
%    调用方法：
% 
%      imageArea = s01_getArea(originalImage)
%      imageArea = s01_getArea(originalImage,'Range',areaRect)
%      imageArea = s01_getArea(originalImage,'ShowEN',true)
%      imageArea = s01_getArea(originalImage,'SaveEN',true,'SavePath','/path')
%      imageArea = s01_getArea(originalImage,'ShowEN',true,'SaveEN',true,'SavePath','/path')
%      imageArea = s01_getArea(image,'Range',areaRect,'SaveEN',true,'originalImage_name',image_name);

function imageRoi = s01_getArea(originalImage,varargin)
    p = inputParser;                    % 函数的输入解析器
    addParameter(p,'Range',false);      % 设置figure显示使能标志初始值
    addParameter(p,'ShowEN',true);      % 设置figure显示使能标志初始值
    addParameter(p,'SaveEN',false);     % 设置区域图像保存使能标志初始值
    addParameter(p,'originalImage_name','roi');     % 设置区域图像的名称
    addParameter(p,'SavePath','C:\Users\yjh\Desktop\TestPicture\ROI\');      % 设置区域图像保存路径初始值
    parse(p,varargin{:});               % 对输入变量进行解析，如果检测到前面的变量被赋值，则更新变量取值
    Range = p.Results.Range;            % figure显示使能标志
    ShowEN = p.Results.ShowEN;          % figure显示使能标志
    SaveEN = p.Results.SaveEN;          % 区域图像保存使能标志
    originalImage_name  = p.Results.originalImage_name;          % 区域图像名称标志
    
    SavePath = p.Results.SavePath;      % 区域图像保存使能标志
 
   
    if(isstruct(Range))%判断是否为结构体
        roix =round((Range.x):(Range.x+ Range.w));
        roiy = round((Range.y):(Range.y+Range.h)); 
        imageRoi=originalImage(roiy,roix,:);
       
        
    else
        imageRoi=originalImage;
       
    end
    
    if(ShowEN)
       figure(101); imshow(imageRoi);
    
       
    end
    
    if(SaveEN)
        imwrite(imageRoi,strcat(SavePath,originalImage_name ,'.bmp'));
     
    end
        
        
        
    
   
    
    
    
end