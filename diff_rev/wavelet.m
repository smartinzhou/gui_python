function eq = wavelet(inputdata)
    [H,W] = size(inputdata);
    Diffimae = double(inputdata(1:floor(H/4)*4,1:floor(W/4)*4));
    [row,rol]=size(Diffimae);
    odd=Diffimae(:,1:2:rol-1);%odd
    even=Diffimae(:,2:2:rol);%even
     %------行变换------%
    H1(:,1)=odd(:,1)-even(:,1);
    H1(:,2:rol/2)=odd(:,2:rol/2)-(even(:,1:rol/2-1)+even(:,2:rol/2))/2;%高频
    L1(:,1:rol/2-1)=even(:,1:rol/2-1)+(H1(:,1:rol/2-1)+H1(:,2:rol/2)+2)/4;%低频
    L1(:,rol/2)=odd(:,end)+1/2*(even(:,end)+1);
    %行变换结束
    trans_first=[L1,H1];
%     figure;imshow(trans_first);
    %------列变换------%
    odd2=trans_first(1:2:row-1,:);%odd
    even2=trans_first(2:2:row,:);%even
    H11(1,:)=odd2(1,:)-even2(1,:);
    H11(2:row/2,:)=odd2(2:row/2,:)-(even2(1:row/2-1,:)+even2(2:row/2,:))/2;%高频
    L11(1:row/2-1,:)=even2(1:row/2-1,:)+(H11(1:row/2-1,:)+H11(2:row/2,:)+2)/4;%低频
    L11(row/2,:)=odd2(end,:)+1/2*(even2(end,:)+1);
    %一维变换结束
    dim_first=[L11;H11];
    %------第二次------%
    %------行变换------%
    odd3=dim_first(:,1:2:rol/2-1);%odd
    even3=dim_first(:,2:2:rol/2);%even
    H2(:,1)=odd3(:,1)-even3(:,1);
    H2(:,2:rol/4)=odd3(:,2:rol/4)-(even3(:,1:rol/4-1)+even3(:,2:rol/4))/2;%高频
    L2(:,1:rol/4-1)=even3(:,1:rol/4-1)+(H2(:,1:rol/4-1)+H2(:,2:rol/4)+2)/4;%低频
    L2(:,rol/4)=odd3(:,end)+1/2*(even3(:,end)+1);
    %二维行变换结束
    trans_second=[L2,H2];
    %figure;imshow(trans_second);
    %------列变换------%
    odd4=trans_second(1:2:row/2-1,:);%odd
    even4=trans_second(2:2:row/2,:);%even
    H22(1,:)=odd4(1,:)-even4(1,:);
    H22(2:row/4,:)=odd4(2:row/4,:)-(even4(1:row/4-1,:)+even4(2:row/4,:))/2;%高频
    L22(1:row/4-1,:)=even4(1:row/4-1,:)+(H22(1:row/4-1,:)+H22(2:row/4,:)+2)/4;%低频
    L22(row/4,:)=odd4(end,:)+1/2*(even4(end,:)+1);%二维变换结束
    [~,Ly]=size(L22);
    LL2=L22(:,1:Ly/2);%二维低频
    HL2=L22(:,Ly/2+1:Ly);%二维高频1
    LH2=H22(:,1:Ly/2);%二维高频2
    HH2=H22(:,Ly/2+1:Ly);%二维高频3
    %LL2
    ELL2=sum(sum(LL2.^2));
    %HL2
    EHL2=sum(sum(HL2.^2));
    %LH2
    ELH2=sum(sum(LH2.^2));
    %HH2
    EHH2=sum(sum(HH2.^2));
%     eq=(EHL2+ELH2+EHH2)/ELL2;
    eq=(EHL2+ELH2+EHH2)/(ELL2);
end