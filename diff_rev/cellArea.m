%功能：获取包含细胞区域的数目，以及细胞区域的坐标
%参数：
%   'ShowEN': figure显示使能标志，false(default)/true
%    'SaveEN: 区域图像保存使能标志，false(default)/true

%输出
%CellRange包含相应区域的最小矩形,返回的参数是 [y，x，width，height] 
%m  连通域数目

% 调用方法：
%[CellRange] = cellArea(img);
%[CellRange] = cellArea(img,'ShowEN',false);
%[CellRange] = cellArea(img,'ShowEN',false,'T1',100);

function [centroid,m,status] = cellArea(img,varargin)
 p = inputParser;
 addParameter(p,'ShowEN',true);     % 设置figure显示使能标志初始值
 addParameter(p,'T1',150);
%  addParameter(p,'corrosion',false);

 parse(p,varargin{:});       % 对输入变量进行解析，如果检测到前面的变量被赋值，则更新变量取值
 ShowEN = p.Results.ShowEN;          % figure显示使能标志
 T1 = p.Results.T1;
% corrosion = p.Results.corrosion;

BWimg = img;
[width,height]=size(img);
%二值化
% T1=75;  %二值化阈值
for i=1:width
    for j=1:height
        if(img(i,j)<T1)
            BWimg(i,j)= 255;
        else 
            BWimg(i,j)= 0;
        end
    end
end
if(ShowEN)
    figure(1);imshow(BWimg);
end

% 开运算
se=strel('disk',5);

     
BWimg = imopen(BWimg,se);

 
     



% %统计标注连通域
%使用外接矩形框选连通域，并使用形心确定连通域位置
[l,m] = bwlabel(BWimg);%l是和BW大小相同矩阵，包含了标记了BW中每个连通区域的类别标签
status=regionprops(l,'BoundingBox');%包含相应区域的最小矩形,返回的参数是 [y，x，width，height] ,
centroid = regionprops(l,'Centroid');%每个区域的中心
% EquivDiameter = regionprops(l,'EquivDiameter');
% all = regionprops(l,'all');

if(ShowEN)
    figure(2);imshow(img);
end
if(ShowEN)
    for i=1:m
        rectangle('position',status(i).BoundingBox,'edgecolor','r');
        text(centroid(i,1).Centroid(1,1)-15,centroid(i,1).Centroid(1,2)-15, num2str(i),'Color', 'r') 
%       roiy=round(status(i).BoundingBox(2):(status(i).BoundingBox(2)+status(i).BoundingBox(4)));%获取细胞区域时使用
%       roix=round(status(i).BoundingBox(1):(status(i).BoundingBox(1)+status(i).BoundingBox(3)));%获取细胞区域时使用
    end
end


end
