#===============================================================
#   Finding something good without looking for it!
#
#   File_Name   ：Macro_Variable.py
#   Author_Name ：HuJinzhou
#   Created Time：2023年10月31日
#   Description ：
#                   定义一些GUI界面会用到的宏变量
#===============================================================

MESS_INFO_DEBUG = False      # 控制打印数据输出debug

PORT_NAME = 'COM3'          # 绑定的串口

# PATH
SERIAL_FUN_PATH = "./mvcamera"      # py调用m函数所在的路径
DIFF_REV_PATH = "./diff_rev"        # matlab衍射恢复函数路径
SAVE_OPEN_IMG = './diff_rev/picture/img_show.png'       #! 保存显示的图像路径
DIFFED_IMG_PATG = './diff_rev/picture/wbc-1390.bmp'            #! 保存衍射恢复处理后的图像路径

# root窗口主要组件
ROOT_ICON_PATH = './icon/lovelearning.ico'
ROOT_WINDOW_TITLE = 'Cultivation Monitoring System Control'
ROOT_WIDTH = 570
ROOT_HEIGHT = 1080

# Frame1 主要组件
UVC_IMG_BLUE = './icon/icon_cultivate/uvc_4040.gif'
UVC_IMG_PURPLE = './icon/icon_cultivate/uvc_respond_4040.gif'
LIGHT_IMG_BLUE = './icon/icon_cultivate/light.gif'
LIGHT_IMG_YELLOW = './icon/icon_cultivate/light_respond.gif'
BUZZER_IMG_BLUE = './icon/icon_cultivate/buzzer.gif'
BUZZER_IMG_RED = './icon/icon_cultivate/buzzer_respond.gif'