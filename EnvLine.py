#=================================================================
#   Finding something good without looking for it!
#
#   File_Name   ：EnvLine.py
#   Author_Name ：HuJinzhou
#   Created Time：2023年10月31日
#   Description ：
#                   实时动态绘制曲线：
#=================================================================


############################################################
##          使用matplotlib创建图表,并显示在tk窗口           ##
############################################################
from matplotlib.pylab import mpl
import tkinter as tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.animation as animation

mpl.rcParams['font.sans-serif'] = ['SimHei']  # 中文显示
mpl.rcParams['axes.unicode_minus'] = False  # 负号显示


# 实时动态曲线绘制
class EnvLine():
    def __init__(self, para_root, rhData, tempData, platData, pdmsData, waterData):
        self.root = para_root       # 主窗口
        self.canvas = tk.Canvas(self.root,bg='#efefef',width=400,height=400) # 创建一块显示图形的画布
        self.figure = self.create_matplotlib(rhData,tempData,platData,pdmsData,waterData) # 返回matplotlib所画图形的figure对象
        self.showGraphIn(self.figure)               # 将figure显示在tkinter窗体上面

    # 生成figure
    def create_matplotlib(self,rhData,tempData,platData,pdmsData,waterData):
        # 创建绘图对象curve
        f = plt.figure(num=1, figsize=(5,5), dpi=100, edgecolor='#1e2732', facecolor='#efefef', frameon=True)

        # 创建一幅子图
        self.fig11 = plt.subplot(1,1,1)
        self.fig11.set_facecolor('#000000')        # 设置matplotlib背景色（纯黑）不能直接写在一起

        x = [i for i in range(len(tempData))]
        y_rh    = [i for i in rhData]
        y_temp  = [i for i in tempData]
        y_plat  = [i for i in platData]
        y_pdms  = [i for i in pdmsData]
        y_water = [i for i in waterData]

        # a1 = self.fig11.add_axes([0,0,1,1])          # 指定了一个有数值范围限制的绘图区域
        self.fig11.set_ylim(20,90)                     # FIXME：给y轴设置变量范围
        # 绘制环境湿度、环境温度、平台温度、pdms温度、水温曲线
        self.line_rh, = self.fig11.plot(x,y_rh, color='green', label="rh")
        self.line_temp, = self.fig11.plot(x,y_temp, color='#fac03d', label="temp")
        self.line_plat, = self.fig11.plot(x,y_plat, color='#c12c1f', label="plat")
        self.line_pdms, = self.fig11.plot(x,y_pdms, color='#f5f3f2', label="pdms")
        self.line_water, = self.fig11.plot(x,y_water, color='#87c0ca', label="water")

        def setLabel(fig, title, titleColor='red'):
            fig.set_title(title+'环境温湿度', color=titleColor)     # 设置标题
            fig.set_xlabel('时间(/s)')          # 设置X轴标签
            fig.set_ylabel('温湿度(/℃&RH)')     # 设置Y轴标签
        setLabel(self.fig11, '培养监测系统')
        f.tight_layout()                # 自动紧凑布局

        # 更新figure
        def updataMeltGraph():
            x = [i for i in range(len(tempData))]
            y_rh    = [i for i in rhData]
            y_temp  = [i for i in tempData]
            y_plat  = [i for i in platData]
            y_pdms  = [i for i in pdmsData]
            y_water = [i for i in waterData]

            self.line_rh.set_data(x, y_rh)          # 更新line_rh曲线数据
            self.line_temp.set_data(x, y_temp)      # 同时更新line_temp曲线数据
            self.line_plat.set_data(x, y_plat)      # 更新line_plat曲线数据
            self.line_pdms.set_data(x, y_pdms)      # 同时更新line_pdms曲线数据
            self.line_water.set_data(x, y_water)    # 同时更新line_water曲线数据

            # 增加下面的以解决更新x数据后，但未更新绘图范围
            # self.fig11.relim()
            # self.fig11.autoscale_view()
            # plt.draw()
            return self.line_rh, self.line_temp, self.line_plat, self.line_pdms, self.line_water# 将需要动态变化的内容return即可
        # 显示图例
        plt.legend((self.line_rh, self.line_temp, self.line_plat, self.line_pdms, self.line_water), ['rh', 'temp', 'plat', 'pdms', 'water'], loc='best')
        # 同一画布多条曲线同时动画
        anim = animation.FuncAnimation(fig=f, func=updataMeltGraph(), interval=1000, blit=True)
        return f

    # 把fig显示到tkinter
    def showGraphIn(self, figure):
        # 把绘制的图形显示到tkinter窗口上
        self.canvas = FigureCanvasTkAgg(figure, self.root)
        self.canvas.draw()
        # self.canvas.get_tk_widget().grid(row=1,column=2,padx=2)
        self.canvas.get_tk_widget().grid(row=1,column=0,columnspan=3)