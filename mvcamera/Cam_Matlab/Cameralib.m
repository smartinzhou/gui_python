%% Acquisition Regularly from MindVision Device 

clear
clear global
clc

clear classes

%ͼƬ������ʽ
NameList={['year-month-day-hour-minute-second.bmp']
    ,['year-month-day-hour-minute.bmp']
    ,['year-month-day-hour.bmp']
    };

% pyversion C:\Users\Administrator\AppData\Local\Programs\Python\Python36\python.exe

SavePath = 'data\';
SaveName = 2; %ѡ��������ʽ
Timestep = 50; %����ɼ�ʱ��
Timeunit = [1 0 0];% s 1  m 60  h 3600

% S1_Wat = 45;  % ʪ��
% PDMS_Temp =  37; % PDMS�¶�
% Water_Temp =  30; % ˮ���¶�
% Plane_Temp =  30; % ƽ̨�¶�
% UVC_time = 5; % min
% light_mode = 1; % ���Դģʽ��0 ����;  1 �Զ�
% % UVC_EN = 1;
% % LIGHT_EN = 1;
% % BUZZER_EN = 1;
% sysdata = [char(S1_Wat)  char(PDMS_Temp) char(Water_Temp)  char(Plane_Temp) ...
%            char(UVC_time)  char(light_mode) ];

if count(py.sys.path,'') == 0
    insert(py.sys.path,int32(0),'');
end

% com = uart();

% uartSentFCN(com,['{' sysdata char(0)  char(0) char(0) '}']);

obj = py.importlib.import_module('mvsdk'); 
py.importlib.reload(obj);
obj = py.importlib.import_module('Camera_Matlab'); 
py.importlib.reload(obj);

disp(['-> MindVision Device List']);
DevList = py.mvsdk.CameraEnumerateDevice();
nDev = size(DevList,2);
Device = 1;
if nDev < 1
    disp(['      ' 'No camera was found!']);
end
DevCell = cell(DevList);
DeviceName = {};
for i=1:nDev
    dev = DevCell{1,i};
    DeviceName{i} = char(uint8(dev.acFriendlyName));
    disp(['      ' num2str(i) '.' DeviceName{i}]);
end
Device = input('-> Choose MindVision Device:');

% �����
disp(['->Open Camera:' DeviceName{Device}]);
hCamera = py.Camera.OpenCamera(py.int(Device));

% ��SDK�ڲ�ȡͼ�߳̿�ʼ����
disp('->��SDK�ڲ�ȡͼ�߳̿�ʼ����');
py.mvsdk.CameraPlay(hCamera)

% ��ȡ�����������  获取相机特性描述
disp('->��ȡ�����������');
cap = py.mvsdk.CameraGetCapability(hCamera);

% �ж��Ǻڰ�������ǲ�ɫ��� 判断是黑白相机还是彩色相机
monoCamera = (cap.sIspCapacity.bMonoSensor ~= 0);

% �ڰ������ISPֱ�����MONO���ݣ���������չ��R=G=B��24λ�Ҷ�
% 黑白相机让ISP直接输出MONO数据，而不是扩展成R=G=B的24位灰度
if monoCamera
    py.mvsdk.CameraSetIspOutFormat(hCamera, py.mvsdk.CAMERA_MEDIA_TYPE_MONO8)
end
% ���ģʽ�л��������ɼ� 相机模式切换成连续采集
py.mvsdk.CameraSetTriggerMode(hCamera, py.int(0))

disp(['-> Support Resolution:']);
disp(['      ' '1.2592X1944 ���']);
disp(['      ' '2.�Զ���']);
nRes = input('-> Choose Resolution:');
if nRes ==2
    re = py.mvsdk.CameraCustomizeResolution(hCamera);
    py.mvsdk.CameraSetImageResolution(hCamera,re);
end

% �ֶ��ع⣬�ع�ʱ��30ms
disp(['-> �ع�']);
disp(['      ' '1.�Զ��ع� 自动']);
disp(['      ' '2.�ֶ��ع� 手动']);
nExp = input('-> Choose Exposure Mode:');
if nExp ==1
    py.mvsdk.CameraSetAeState(hCamera, py.int(1))
end
if nExp ==2
    ExposureTime = py.int(input('-> Input Exposure Time(us):'));  %um
    py.mvsdk.CameraSetAeState(hCamera, py.int(0))
    py.mvsdk.CameraSetExposureTime(hCamera, ExposureTime)
    AnalogGain = py.int(input('-> Input AnalogGain(int):'));
    py.mvsdk.CameraSetAnalogGain(hCamera,AnalogGain)
end

% ����RGB buffer����Ĵ�С������ֱ�Ӱ�����������ֱ���������
% 计算RGB buffer所需的大小，这里直接按照相机的最大分辨率来分配
disp('->����RGB buffer����Ĵ�С');
if monoCamera
    FrameBufferSize = cap.sResolutionRange.iWidthMax * cap.sResolutionRange.iHeightMax;
else
    FrameBufferSize = cap.sResolutionRange.iWidthMax * cap.sResolutionRange.iHeightMax*3;
end
py.mvsdk.CameraUnInit(hCamera);

FUNEN = [0 0 0 0 0];
while(1)
    disp(['--> Fuctions:']);
    disp(['          1.preview']);
    disp(['          2.snapshot']);
    disp(['          3.Timing Acquisition']);
    disp(['          4.Exit']);
    fun = input('--> Choose:');
    if( sum(FUNEN)~= 0 )
        disp(['--> Please Close the last Function!!!']);
    end
    FUNEN(fun) = 1;
    % Show frame one by one 
    if(FUNEN(1))
        % preview(cam);
        FUNEN(1) = 0;
        break;
    end
    % Acquisition a frame
    if(FUNEN(2))
        hCamera = py.Camera.OpenCamera(py.int(Device));
        py.mvsdk.CameraPlay(hCamera)
        py.mvsdk.CameraSetTriggerMode(hCamera, py.int(0))
        if monoCamera
            py.mvsdk.CameraSetIspOutFormat(hCamera, py.mvsdk.CAMERA_MEDIA_TYPE_MONO8)
        end
        Data = py.Camera.GetFrame(hCamera, py.int(FrameBufferSize));
        py.mvsdk.CameraUnInit(hCamera);
        image = nparray2mat(Data);
        figure(100);imshow(image./255);
        FUNEN(2) = 0;
    end
    if(FUNEN(3))
        disp(['--> Timing Acquisition:']);
        disp(['          Path:' SavePath]);
        disp(['          Name:' NameList{SaveName}]);
        disp(['          Time Step:' NameList{SaveName}]);
        disp(['          1.Change Save Path']);
        disp(['          2.Change Save Name']);
        disp(['          3.Start']);
        fun = input('--> Choose:');
        num = 0;
        while(fun == 3)

            uartSentFCN(com,['{' sysdata char(0)  char(1) char(0) '}']);
            pause(11);
            hCamera = py.Camera.OpenCamera(py.int(Device));
            py.mvsdk.CameraPlay(hCamera)
            py.mvsdk.CameraSetTriggerMode(hCamera, py.int(0))
            if monoCamera
                py.mvsdk.CameraSetIspOutFormat(hCamera, py.mvsdk.CAMERA_MEDIA_TYPE_MONO8)
            end
            Data = py.Camera.GetFrame(hCamera, py.int(FrameBufferSize));
            img = nparray2mat(Data)./255;
            py.mvsdk.CameraUnInit(hCamera);
            img2 = imresize(img,0.2);
            figure(100);imshow(img2);
            imgname = [datestr(now,'yyyy-mm-dd-HH-MM-SS') '.bmp'];
            num = num+1;
            disp(['          Num:' num2str(num) '  Image:' imgname]);
            imwrite(img,[SavePath imgname]);
            
            uartSentFCN(com,['{' sysdata char(0)  char(0) char(0) '}']);
            pause(sum(Timestep.*Timeunit)-11);
        end
    end
    if(FUNEN(4))
        py.mvsdk.CameraUnInit(hCamera);
        break;
    end
    % Close device;
    % clear cam
    % clear global
end
