function uartReceiveFCN(obj,event)
    data = fread(obj,12);
    %global S1_Wat S2_Wat S1_Temp S2_Temp Plane_Temp PDMS_Temp Water_Temp
    global envir
    envir.S1_Wat = [envir.S1_Wat data(2)];
    envir.S2_Wat = [envir.S2_Wat data(3)];
    envir.S1_Temp =  [envir.S1_Temp data(4)];
    envir.S2_Temp =  [envir.S2_Temp data(5)];
    
    envir.Plane_Temp =  [envir.Plane_Temp data(6) + data(7)/255.0];
    envir.PDMS_Temp =  [envir.PDMS_Temp data(8) + data(9)/255.0];
    envir.Water_Temp =  [envir.Water_Temp data(10) + data(11)/255.0];
    
    % envir.S1_Wat = [envir.S1_Wat hex2dec(num2str(data(2)))];
    % envir.S2_Wat = [envir.S2_Wat hex2dec(num2str(data(3)))];
    % envir.S1_Temp =  [envir.S1_Temp hex2dec(num2str(data(4)))];
    % envir.S2_Temp =  [envir.S2_Temp hex2dec(num2str(data(5)))];
    % 
    % envir.Plane_Temp =  [envir.Plane_Temp hex2dec(num2str(data(6))) + hex2dec(num2str(data(7)))/255.0];
    % envir.PDMS_Temp =  [envir.PDMS_Temp hex2dec(num2str(data(8))) + hex2dec(num2str(data(9)))/255.0];
    % envir.Water_Temp =  [envir.Water_Temp hex2dec(num2str(data(10))) + hex2dec(num2str(data(11)))/255];
    
    save envir.mat envir
    %disp(['S1_Wat:' num2str(envir.S1_Wat(end))  ... 
    %    '%  S2_Wat:' num2str(envir.S2_Wat(end)) ...
    %    '%  S1_Temp:' num2str(envir.S1_Temp(end)) ...
    %    '��  S2_Temp:' num2str(envir.S2_Temp(end)) ...
    %    '��']);
%     disp(['Plane_Temp:' num2str(envir.Plane_Temp(end)) ...
%         '��  PDMS_Temp:' num2str(envir.PDMS_Temp(end)) ...
%         '��  Water_Temp:' num2str(envir.Water_Temp(end)) ...
%         '��']);
    figure(1);hold on;
    plot(envir.S1_Wat,'b+');
    plot(envir.S2_Wat,'b*');
    plot(envir.S1_Temp,'r+');
    plot(envir.S2_Temp,'r*');
    plot(envir.Plane_Temp,'g+');
    plot(envir.PDMS_Temp,'g.');
    plot(envir.Water_Temp,'g*');
    hold off;
    %fclose(obj)
end







