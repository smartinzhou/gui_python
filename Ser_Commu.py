#===============================================================
#   Finding something good without looking for it!
#
#   File_Name   ：ser_conn.py
#   Author_Name ：HuJinzhou
#   Created Time：2023年10月31日
#   Description ：
#                   usb串口通信：
#                       1. 串口传参连接
#                       2. 串口的数据实时read & write
#===============================================================

import serial
import Macro_Variable as mvar

class Ser_Commu:
    def __init__(self):
        self.rh_read = 0
        self.temp_read = 0
        self.plat_read = 0
        self.pdms_read = 0
        self.water_read = 0
        # self.w_data = []

    def ser_conn(self, para_port):
        # global ser
        # # 通过matlab.engine选择串口打开
        # eng = matlab.engine.start_matlab("-desktop")
        # eng.cd(mvar.SERIAL_FUN_PATH,nargout=0)       # 进入指定路径下找matlab的函数
        # ser = eng.uart(nargout=0)               # 调用matlab的串口自定义函数，选择打开串口

        ser = serial.Serial(para_port, 9600, timeout=5)
        if ser.isOpen():
            print("Success Open Serial!",'\n', ser.name)
        else:
            print("!!!!!! FAIL OPEN SERIAL !!!!!!")

        return ser

    def usb_read(self, para_ser):
        # USB串口读数据
        a=para_ser.read_until(expected=b'}')
        self.rh_read = (a[1] + a[2])/2               # 环境湿度，两个湿度传感器取平均
        self.temp_read = (a[3] + a[4])/2             # 环境温度，两个温度传感器取平均
        self.plat_read = a[5] + round((a[6]/255),2)      # 平台温度，整数与小数部分数据，保留两位小数
        self.pdms_read = a[7] + round((a[8]/255),2)      # PDMS温度
        self.water_read = a[9] + round((a[10]/255),2)    # 水温

        return self.rh_read, self.temp_read, self.plat_read, self.pdms_read, self.water_read

    def usb_write(self, *args, para_ser):
        w_data = list(args)             # 将元组转化为数组列表
        para_ser.write(w_data)          # 作为整体发送